<?php

namespace Ls\FaqBundle\Controller;

use Ls\FaqBundle\Entity\FaqCategories;
use Ls\FaqBundle\Form\FaqAdminCategoriesType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminCategoriesController extends Controller {
    private $faqr_limit_name = 'admin_faq_categories_faq_categoriesr_limit';

    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');

        $page = $request->query->get('faq', 1);
        if ($session->has($this->faqr_limit_name)) {
            $limit = $session->get($this->faqr_limit_name);
        } else {
            $limit = 15;
            $session->set($this->faqr_limit_name, $limit);
        }

        $query = $em->createQueryBuilder()
            ->select('e')
            ->from('LsFaqBundle:FaqCategories', 'e')
            ->getQuery();

        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $page,
            $limit,
            array(
                'defaultSortFieldName' => 'e.title',
                'defaultSortDirection' => 'asc',
            )
        );
        $entities->setTemplate('LsCoreBundle:Backend:paginator.html.twig');

        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl('ls_admin_faq_categories'));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Kategorie pytań', $this->get('router')->generate('ls_admin_faq_categories'));

        return $this->render('LsFaqBundle:AdminCategories:index.html.twig', array(
            'page' => $page,
            'limit' => $limit,
            'entities' => $entities,
        ));
    }

    public function newAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = new FaqCategories();

        $form = $this->createForm(FaqAdminCategoriesType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_faq_categories_new'),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz i wróć na listę'));
        $form->add('submit_and_new', SubmitType::class, array('label' => 'Zapisz i dodaj następny'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $sitemap = $this->get('ls_core.sitemap');
            $sitemap->generate();

            $this->get('session')->getFlashBag()->add('success', 'Dodanie Kategorie pytań zakończone sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_faq_categories_edit', array('id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_faq_categories'));
            }
            if ($form->get('submit_and_new')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_faq_categories_new'));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Kategorie pytań', $this->get('router')->generate('ls_admin_faq_categories'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_faq_categories_new'));

        return $this->render('LsFaqBundle:AdminCategories:new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function editAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsFaqBundle:FaqCategories')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Faq entity.');
        }

        $form = $this->createForm(FaqAdminCategoriesType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_faq_categories_edit', array('id' => $entity->getId())),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz zmiany'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz zmiany i zamknij'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $sitemap = $this->get('ls_core.sitemap');
            $sitemap->generate();

            $this->get('session')->getFlashBag()->add('success', 'Aktualizacja podstrony zakończona sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_faq_categories_edit', array('id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_faq_categories'));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Kategorie pytań', $this->get('router')->generate('ls_admin_faq_categories'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_faq_categories_edit', array('id' => $entity->getId())));

        return $this->render('LsFaqBundle:AdminCategories:edit.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView()
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsFaqBundle:FaqCategories')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Faq entity.');
        }

        $em->remove($entity);
        $em->flush();

        $sitemap = $this->get('ls_core.sitemap');
        $sitemap->generate();

        $this->get('session')->getFlashBag()->add('success', 'Usunięcie podstrony zakończone sukcesem.');

        return new Response('OK');
    }

    public function batchAction(Request $request) {
        $ids = $request->request->get('ids');
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = 'Czy na pewno chcesz ';
            switch ($action) {
                case 'delete':
                    $message .= 'usunąć ';
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'element?';
                    break;
                case 2:
                case 3:
                case 4:
                    $message .= 'elementy?';
                    break;
                default:
                    $message .= 'elementów?';
                    break;
            }

            $breadcrumbs = $this->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
            $breadcrumbs->addItem('Kategorie pytań', $this->get('router')->generate('ls_admin_faq_categories'));
            $breadcrumbs->addItem('Potwierdzenie', $this->get('router')->generate('ls_admin_faq_categories_batch'));

            return $this->render('LsFaqBundle:AdminCategories:batch.html.twig', array(
                'message' => $message,
                'action' => $action,
                'ids' => implode(',', $ids),
            ));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_faq_categories'));
        }
    }

    public function batchExecuteAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $ids = explode(',', $request->request->get('ids'));
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = '';
            switch ($action) {
                case 'delete':
                    $message .= 'Usunięcie ';
                    $qb = $em->createQueryBuilder();
                    $query = $qb->select('e')
                        ->from('LsFaqBundle:FaqCategories', 'e')
                        ->add('where', $qb->expr()->in('e.id', $ids))
                        ->getQuery();

                    $iterableResult = $query->iterate();
                    while (($row = $iterableResult->next()) !== false) {
                        $em->remove($row[0]);
                        $em->flush();
                    }

                    $sitemap = $this->get('ls_core.sitemap');
                    $sitemap->generate();

                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'elementu ';
                    break;
                default:
                    $message .= 'elementów ';
                    break;
            }
            $message .= 'zakończone sukcesem ';

            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirect($this->generateUrl('ls_admin_faq_categories'));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_faq_categories'));
        }
    }

    public function setLimitAction(Request $request) {
        $session = $this->container->get('session');

        $limit = $request->request->get('limit');
        $session->set($this->faqr_limit_name, $limit);

        return new Response('OK');
    }
}
