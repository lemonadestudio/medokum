-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: medicaltranslations
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.21-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faq`
--

DROP TABLE IF EXISTS `faq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E8FF75CC12469DE2` (`category_id`),
  CONSTRAINT `FK_E8FF75CC12469DE2` FOREIGN KEY (`category_id`) REFERENCES `faq_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faq`
--

LOCK TABLES `faq` WRITE;
/*!40000 ALTER TABLE `faq` DISABLE KEYS */;
INSERT INTO `faq` VALUES (4,'Jak dodać zamówienie?','jak-dodac-zamowienie',0,'Przez stronę.','<p>\r\n	Przez stronę.</p>',0,'Jak dodać zamówienie?','dodać, zamówienie?, Przez, stronę','Przez stronę.','2017-05-30 13:33:19','2017-06-01 15:34:17',2),(5,'Czy muszę się rejestrować?','czy-musze-sie-rejestrowac',0,'Nie ma takiej potrzeby.','<p>\r\n	Nie ma takiej potrzeby.</p>',0,'Czy muszę się rejestrować?','muszę, się, rejestrować?, takiej, potrzeby','Nie ma takiej potrzeby.','2017-05-30 13:33:39','2017-06-01 11:05:47',1);
/*!40000 ALTER TABLE `faq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faq_categories`
--

DROP TABLE IF EXISTS `faq_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faq_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faq_categories`
--

LOCK TABLES `faq_categories` WRITE;
/*!40000 ALTER TABLE `faq_categories` DISABLE KEYS */;
INSERT INTO `faq_categories` VALUES (1,'Proste pytania','',NULL,NULL,NULL,NULL,'0000-00-00 00:00:00',NULL),(2,'Trudne pytania','',NULL,NULL,NULL,NULL,'0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `faq_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file`
--

DROP TABLE IF EXISTS `file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8C9F36108D9F6D38` (`order_id`),
  KEY `IDX_8C9F3610C54C8C93` (`type_id`),
  CONSTRAINT `FK_8C9F36108D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  CONSTRAINT `FK_8C9F3610C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `files_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file`
--

LOCK TABLES `file` WRITE;
/*!40000 ALTER TABLE `file` DISABLE KEYS */;
INSERT INTO `file` VALUES (1,'file1','0000-00-00 00:00:00','/upload/files/2/file.pdf',2,NULL,NULL),(2,'file002','0000-00-00 00:00:00','/upload/files/2/file.pdf',2,NULL,NULL),(67,NULL,'2017-07-11 15:10:36',NULL,NULL,NULL,'C:\\xampp\\tmp\\php5C9B.tmp'),(68,'order-file-5964ce4ca7a23.xlsx','2017-07-11 15:10:36',NULL,NULL,NULL,NULL),(69,NULL,'2017-07-11 15:15:29',NULL,NULL,NULL,'C:\\xampp\\tmp\\phpD621.tmp'),(70,'order-file-5964cf71e2a2f.xlsx','2017-07-11 15:15:29',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files_types`
--

DROP TABLE IF EXISTS `files_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files_types`
--

LOCK TABLES `files_types` WRITE;
/*!40000 ALTER TABLE `files_types` DISABLE KEYS */;
INSERT INTO `files_types` VALUES (1,'Historia zdrowia i choroby','2017-05-15 12:27:32',3.00),(2,'Wypis ze szpitala','2017-05-15 12:38:45',12.00),(3,'Zwolnienie lekarskie','2017-05-15 12:44:42',6.00);
/*!40000 ALTER TABLE `files_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gallery`
--

DROP TABLE IF EXISTS `gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `on_list` tinyint(1) NOT NULL,
  `attachable` tinyint(1) NOT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gallery`
--

LOCK TABLES `gallery` WRITE;
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gallery_photo`
--

DROP TABLE IF EXISTS `gallery_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gallery_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `arrangement` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F02A543B4E7AF8F` (`gallery_id`),
  CONSTRAINT `FK_F02A543B4E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gallery_photo`
--

LOCK TABLES `gallery_photo` WRITE;
/*!40000 ALTER TABLE `gallery_photo` DISABLE KEYS */;
/*!40000 ALTER TABLE `gallery_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_item`
--

DROP TABLE IF EXISTS `menu_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route_parameters` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `onclick` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blank` tinyint(1) DEFAULT NULL,
  `arrangement` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D754D550727ACA70` (`parent_id`),
  CONSTRAINT `FK_D754D550727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `menu_item` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_item`
--

LOCK TABLES `menu_item` WRITE;
/*!40000 ALTER TABLE `menu_item` DISABLE KEYS */;
INSERT INTO `menu_item` VALUES (1,NULL,'menu_top','route','Co robimy','ls_page_show','{\"slug\":\"co-robimy\"}','/co-robimy',NULL,0,1),(2,NULL,'menu_top','url','Kontakt',NULL,NULL,'#kontakt',NULL,0,3),(3,NULL,'menu_top','url','FAQ',NULL,NULL,'/faq',NULL,0,2);
/*!40000 ALTER TABLE `menu_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) DEFAULT NULL,
  `show_main` tinyint(1) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `published_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1DD399504E7AF8F` (`gallery_id`),
  CONSTRAINT `FK_1DD399504E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` datetime NOT NULL,
  `status` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `kind_id` int(11) DEFAULT NULL,
  `types` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `finish_date` datetime NOT NULL,
  `priority_id` int(11) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `format` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `pages` int(11) NOT NULL,
  `owner_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_id` int(11) NOT NULL,
  `expiration` datetime NOT NULL,
  `ccv` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E52FFDEE30602CA9` (`kind_id`),
  KEY `IDX_E52FFDEE497B19F9` (`priority_id`),
  CONSTRAINT `FK_E52FFDEE30602CA9` FOREIGN KEY (`kind_id`) REFERENCES `orders_kind` (`id`),
  CONSTRAINT `FK_E52FFDEE497B19F9` FOREIGN KEY (`priority_id`) REFERENCES `orders_priority` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'Plik','tomek@lemonadestudio.pl',NULL,'2017-03-14 00:00:00','W realizacji',1,NULL,'0000-00-00 00:00:00',NULL,NULL,'',0,'',0,'0000-00-00 00:00:00',0),(2,'Dokumenty','tomek@lemonadestudio.pl',NULL,'2017-04-04 00:00:00','Zakończone',2,NULL,'0000-00-00 00:00:00',NULL,NULL,'',0,'',0,'0000-00-00 00:00:00',0),(20,'Tomasz Jacek','tomek@lemonadestudio.pl',NULL,'2017-07-11 13:22:36','Przyjęte',1,'Wypis ze szpitala','2017-07-19 00:00:00',3,'Krajobrazowa','wydruk',111,'Tomasz Jacek',1,'2012-01-01 00:00:00',1),(21,'Tomasz Jacek','tomek@lemonadestudio.pl',NULL,'2017-07-11 13:54:08','Przyjęte',1,'Wypis ze szpitala','2017-07-19 00:00:00',3,'Krajobrazowa','wydruk',111,'Tomasz Jacek',1,'2012-01-01 00:00:00',1),(22,'Tomasz Jacek','tomek@lemonadestudio.pl',NULL,'2017-07-11 13:54:45','Przyjęte',1,'Wypis ze szpitala','2017-07-28 00:00:00',2,'Krajobrazowa','wydruk',11,'Tomasz Jacek',11,'2012-01-01 00:00:00',1),(23,'Tomasz Jacek','tomek@lemonadestudio.pl',NULL,'2017-07-11 13:55:16','Przyjęte',1,'Wypis ze szpitala','2017-07-28 00:00:00',2,'Krajobrazowa','wydruk',11,'Tomasz Jacek',11,'2012-01-01 00:00:00',1),(24,'Tomasz Jacek','tomek@lemonadestudio.pl',NULL,'2017-07-11 13:55:38','Przyjęte',1,'Wypis ze szpitala','2017-07-28 00:00:00',2,'Krajobrazowa','wydruk',11,'Tomasz Jacek',11,'2012-01-01 00:00:00',1),(25,'Tomasz Jacek','tomek@lemonadestudio.pl',NULL,'2017-07-11 13:58:32','Przyjęte',1,'Wypis ze szpitala','2017-07-28 00:00:00',2,'Krajobrazowa','wydruk',11,'Tomasz Jacek',11,'2012-01-01 00:00:00',1),(26,'Tomasz Jacek','tomek@lemonadestudio.pl',NULL,'2017-07-11 13:58:40','Przyjęte',1,'Wypis ze szpitala','2017-07-28 00:00:00',2,'Krajobrazowa','wydruk',11,'Tomasz Jacek',11,'2012-01-01 00:00:00',1),(27,'Tomasz Jacek','tomek@lemonadestudio.pl',NULL,'2017-07-11 14:00:45','Przyjęte',1,'Wypis ze szpitala','2017-07-28 00:00:00',2,'Krajobrazowa','wydruk',11,'Tomasz Jacek',11,'2012-01-01 00:00:00',1),(28,'Tomasz Jacek','tomek@lemonadestudio.pl',NULL,'2017-07-11 14:01:13','Przyjęte',1,'Wypis ze szpitala','2017-07-28 00:00:00',2,'Krajobrazowa','wydruk',11,'Tomasz Jacek',11,'2012-01-01 00:00:00',1),(29,'Tomasz Jacek','tomek@lemonadestudio.pl',NULL,'2017-07-11 15:10:36','Przyjęte',1,'Wypis ze szpitala','2017-07-28 00:00:00',2,'Krajobrazowa','wydruk',1,'Tomasz Jacek',1,'2012-01-01 00:00:00',1),(30,'Tomasz Jacek','tomek@lemonadestudio.pl',NULL,'2017-07-11 15:15:29','Przyjęte',1,'Wypis ze szpitala','2017-07-20 00:00:00',2,'Krajobrazowa','wydruk',1,'Tomasz Jacek',1,'2012-01-01 00:00:00',1);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_kind`
--

DROP TABLE IF EXISTS `orders_kind`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_kind` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_kind`
--

LOCK TABLES `orders_kind` WRITE;
/*!40000 ALTER TABLE `orders_kind` DISABLE KEYS */;
INSERT INTO `orders_kind` VALUES (1,'Podstawowa','2017-05-11 13:08:33',9.00,'<ul>\r\n	<li>\r\n		Tłumaczenie najbardziej istotnych fragment&oacute;w dokumentu bez opisu pieczątek i fragment&oacute;w o treści niemedycznej, np. nagł&oacute;wk&oacute;w adresowych, pouczeń, uwag</li>\r\n</ul>'),(2,'Standardowa','2017-05-11 13:09:59',12.00,'<ul>\r\n	<li>\r\n		Tłumaczenie całej treści dokumentu</li>\r\n	<li>\r\n		Zachowanie rozmieszczenia tekstu oryginału&nbsp;</li>\r\n	<li>\r\n		Weryfikacja zgodności tłumaczenia z oryginałem przez drugiego tłumacza</li>\r\n</ul>'),(3,'Kompleksowa','2017-05-11 13:09:24',6.00,'<ul>\r\n	<li>\r\n		Tłumaczenie całej treści dokumentu z opisem pieczątek i fragment&oacute;w o treści niemedycznej</li>\r\n	<li>\r\n		Zachowanie rozmieszczenia tekstu oryginału</li>\r\n</ul>');
/*!40000 ALTER TABLE `orders_kind` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_priority`
--

DROP TABLE IF EXISTS `orders_priority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_priority` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_priority`
--

LOCK TABLES `orders_priority` WRITE;
/*!40000 ALTER TABLE `orders_priority` DISABLE KEYS */;
INSERT INTO `orders_priority` VALUES (1,'Podstawowy',2.00,'0000-00-00 00:00:00'),(2,'Espress',5.00,'0000-00-00 00:00:00'),(3,'Podstawowy',10.00,'0000-00-00 00:00:00');
/*!40000 ALTER TABLE `orders_priority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page`
--

DROP TABLE IF EXISTS `page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_140AB6204E7AF8F` (`gallery_id`),
  CONSTRAINT `FK_140AB6204E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page`
--

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
INSERT INTO `page` VALUES (1,NULL,'Co robimy','co-robimy','co-robimy',0,'Lorem ipsum dolor sit amet dolor. Suspendisse a dolor eu ipsum eget velit. Suspendisse dapibus diam. Fusce nulla ligula sit amet quam. Vestibulum ligula. Praesent quis massa. Nulla mollis tempor risus. Etiam leo ac felis. Curabitur eu urna ligula...','<h1 class=\"naglowek\">\r\n	Lorem ipsum dolor sit amet dolor. Suspendisse a dolor eu ipsum eget velit. Suspendisse dapibus diam. Fusce nulla ligula sit amet quam. Vestibulum ligula. Praesent quis massa. Nulla mollis tempor risus. Etiam leo ac felis. Curabitur eu urna ligula tempor nisl erat volutpat. Phasellus vitae turpis.</h1>\r\n<div>\r\n	Fusce sed nulla in accumsan fringilla neque, vitae erat erat in augue. Duis vel neque quis augue. Nam consectetuer congue augue eget dui dui, non felis. Praesent rutrum. Donec eget odio sit amet, libero. Nam risus pede, cursus a, euismod non, placerat porta ac, pede. Sed vitae ante. Integer eu enim. Class aptent taciti sociosqu ad litora torquent per inceptos hymenaeos. Sed diam placerat at, rhoncus suscipit. Nulla mi vitae neque gravida iaculis ante.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<ul>\r\n	<li>\r\n		Curae, Duis aliquam, risus.</li>\r\n	<li>\r\n		Aliquam erat felis, vestibulum et, ultricies neque ut pede.</li>\r\n	<li>\r\n		Cras ipsum primis in turpis metus tellus, quis molestie a, aliquet vulputate, odio consequat sed, sollicitudin augue a wisi. Phasellus vitae lorem tempus purus.</li>\r\n</ul>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Quisque at augue a odio eget diam justo, eget commodo nec, scelerisque sem. Aenean non mi id eros lacus, suscipit vitae, pellentesque quis, congue ac, vehicula non, ultrices velit, vitae ornare velit ornare dolor ac nibh porta sed, posuere cubilia Curae, Integer mi risus sit amet, consectetuer egestas, nunc sit amet mauris magna, tincidunt mattis eget, dapibus augue egestas blandit, quam. Curabitur sit amet mauris. Donec porttitor vel, dapibus risus vehicula convallis ac, vulputate tortor venenatis tristique, augue a leo. Sed et pede.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Aliquam gravida ullamcorper risus. Nullam aliquet. Morbi nisl nulla nulla, accumsan lectus est pede, at urna ut eros. Ut ac nulla. Morbi tincidunt. Maecenas bibendum ipsum primis in nunc. Maecenas viverra venenatis blandit velit ac odio at eros. Vestibulum laoreet iaculis suscipit. Suspendisse eu nunc ultrices fringilla mollis. Proin vulputate in, dapibus tellus. Praesent gravida wisi eget sem ullamcorper pede bibendum blandit, quam. Aliquam erat volutpat. Curabitur vitae lectus id sollicitudin sed, viverra a, pretium vitae, pellentesque facilisis. Nulla in dui vitae urna. Donec commodo, tortor eros, id lorem. Cras ac pede. Cras sit amet eros. Quisque pharetra, urna semper magna hendrerit risus. Aliquam tempor diam placerat nec, pharetra sem, eleifend neque vitae erat a nulla. Aliquam fermentum leo lobortis.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Nullam justo dolor, varius risus nec tortor venenatis eu, tortor. Praesent gravida ullamcorper id, urna. Vestibulum dapibus non, feugiat congue, aliquet quis, pellentesque dolor. In pretium eros ut leo sed lacus. Aenean libero. Morbi orci. Proin vel metus. Quisque vestibulum. Pellentesque habitant morbi tristique bibendum, tellus. In hac habitasse platea dictumst. Vestibulum quam. Pellentesque placerat. Vivamus posuere vitae, imperdiet ut, gravida ullamcorper ligula ut justo consequat mollis eros nec malesuada vitae, pellentesque non, feugiat est, dapibus nisl. Vestibulum vel risus. Etiam at ipsum dolor ut wisi. Donec at turpis at purus. Quisque in quam elit rhoncus laoreet risus. Sed diam eu odio. Vestibulum elit sed elit laoreet sit amet leo tristique magna tincidunt mauris. Nullam consequat wisi. Maecenas fermentum pede.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Curae, Duis sit amet quam. Pellentesque quam eu libero. Nulla convallis at, egestas sit amet eros. Donec odio tellus felis enim, id nonummy rutrum. In id lectus. Nullam justo euismod pede sed tortor. In neque sollicitudin orci. Integer posuere eget, tincidunt et, felis. Maecenas eget felis. Morbi pede. Fusce gravida, nisl pellentesque ut, fermentum in, purus. Fusce aliquam dictum wisi nibh, fermentum malesuada. Donec enim aliquam vehicula libero purus fringilla non, consectetuer adipiscing felis augue a ante ipsum a ante id eros ut malesuada ultricies. Donec dolor libero ac nisl. Nunc sit amet dignissim id, mattis neque. Suspendisse potenti. Quisque at sem. Aenean aliquet, purus dolor gravida sagittis. Aliquam faucibus tempor, dolor nunc, mollis tempus. Pellentesque mattis ac, semper turpis.</div>',0,'Co robimy','robimy',NULL,'2017-05-23 11:55:54','2017-06-02 13:50:31');
/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_9F74B898EA750E8` (`label`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `setting`
--

LOCK TABLES `setting` WRITE;
/*!40000 ALTER TABLE `setting` DISABLE KEYS */;
INSERT INTO `setting` VALUES (1,'seo_description','Domyślna wartość meta tagu \"description\"','Instalka CMS Lemonade Studio'),(2,'seo_keywords','Domyślna wartość meta tagu \"keywords\"',NULL),(3,'seo_title','Domyślna wartość meta tagu \"title\"','Instalka'),(4,'email_to_contact','Adres/Adresy e-mail (oddzielone przecinkiem) na które wysyłane są wiadomości z formularza kontaktowego','rafalglazar@gmail.com'),(5,'google_recaptcha_secret_key','Google reCAPTCHA - secret key',NULL),(6,'google_recaptcha_site_key','Google reCAPTCHA - site key',NULL);
/*!40000 ALTER TABLE `setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slider`
--

DROP TABLE IF EXISTS `slider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_width` int(11) NOT NULL,
  `photo_height` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_CFC71007EA750E8` (`label`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slider`
--

LOCK TABLES `slider` WRITE;
/*!40000 ALTER TABLE `slider` DISABLE KEYS */;
/*!40000 ALTER TABLE `slider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slider_photo`
--

DROP TABLE IF EXISTS `slider_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slider_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slider_id` int(11) DEFAULT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `arrangement` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9203C87C2CCC9638` (`slider_id`),
  CONSTRAINT `FK_9203C87C2CCC9638` FOREIGN KEY (`slider_id`) REFERENCES `slider` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slider_photo`
--

LOCK TABLES `slider_photo` WRITE;
/*!40000 ALTER TABLE `slider_photo` DISABLE KEYS */;
/*!40000 ALTER TABLE `slider_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salt` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json_array)',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','biuro@lemonadestudio.pl','e40e0bc6f3ebacbed6e0e19b772845a8','Rd/3Txoa82/D9hi5AUbzSaMo0mR7YPs5tVG7TwD12SREfgob9BnzCItP1AhQxf7gi/IHE67DdDYPbMWPZIAROg==',1,NULL,'[\"ROLE_ADMIN\",\"ROLE_ALLOWED_TO_SWITCH\",\"ROLE_USER\"]','2015-07-31 13:46:34',NULL),(2,'tj','tomek@lemonadestudio.pl','eb4c9285d2177a7b763f33d3dfb26708','3T10sVpadabvQNRDpxzsDZ1AtfFPX7PnTqo7SXdD3RKCtLKFomW68Tg4+zPEs+2saOHLgJoWx+Tj6UljBDx/pQ==',1,NULL,'[\"ROLE_USER\"]','2017-05-05 11:11:30',NULL),(3,'medokum','medokum@medokum.pl','f38f48f5e130f8d75ca503c2ef9a9ddc','ghnahsk0fEowLAUlofF/OglQd0mDjhrHxvjuYxzW/FHTjbV9T60ut9XyMnf85E8LJyA18NLfOhmBy82V2t3jjg==',1,NULL,'[\"ROLE_ADMIN\"]','2017-06-23 15:58:44',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-12 10:35:44
