<?php

namespace Ls\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * OrderKind
 * @ORM\Table(name="orders_kind")
 * @ORM\Entity
 */
class OrderKind {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="defaultValue", type="boolean")
     * @var string
     */
    private $default;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $name;
    
    /**
     * @ORM\Column(type="decimal", scale=2)
     * @var decimal
     */
    private $firstPrice;
    
    /**
     * @ORM\Column(type="decimal", scale=2)
     * @var decimal
     */
    private $nextPrice;
    
    /**
     * @ORM\Column(type="text")
     * @var string
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * Constructor
     */
    public function __construct() {
        $this->created_at = new \DateTime();
        $this->status = 'pending';
        $this->files = new ArrayCollection();
        $this->default = false;
    }

    public function __toString()
    {
        if (is_null($this->getName())) {
            return 'NULL';
        }

        return $this->getName();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set default
     *
     * @param integer $default
     *
     * @return OrderPriority
     */
    public function setDefault($default)
    {
        $this->default = $default;
    
        return $this;
    }

    /**
     * Get default
     *
     * @return integer
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return OrderKind
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return OrderKind
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set firstPrice
     *
     * @param integer $firstPrice
     *
     * @return OrderKind
     */
    public function setFirstPrice($firstPrice)
    {
        $this->firstPrice = $firstPrice;
    
        return $this;
    }

    /**
     * Get firstPrice
     *
     * @return integer
     */
    public function getFirstPrice()
    {
        return $this->firstPrice;
    }
    
    /**
     * Set nextPrice
     *
     * @param integer $nextPrice
     *
     * @return OrderKind
     */
    public function setNextPrice($nextPrice)
    {
        $this->nextPrice = $nextPrice;
    
        return $this;
    }

    /**
     * Get nextPrice
     *
     * @return integer
     */
    public function getNextPrice()
    {
        return $this->nextPrice;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return OrderKind
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
}
