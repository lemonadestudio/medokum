<?php

namespace Ls\IndexContentBundle\Extensions;

use Ls\IndexContentBundle\Utils\IndexContent;

class IndexContentExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface {

    protected $index_content;

    function __construct(IndexContent $index_content) {
        $this->index_content = $index_content;
    }

    public function getGlobals() {
        return array(
            'index_content' => $this->index_content,
            'all_index_content' => $this->index_content->all(),
        );
    }

    public function getName() {
        return 'index_content';
    }
}