<?php

namespace Ls\OrderBundle\Controller;

use Ls\OrderBundle\Entity\Order;
use Ls\OrderBundle\Entity\File;
use Ls\OrderBundle\Form\OrderType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Session\Session;
use Ls\OrderBundle\Utils\PayPal;
use Ls\CoreBundle\Utils\CardScheme;
use PayPal\Api\Payment;

class FrontController extends Controller {
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = new Order();

        $form = $this->createForm(OrderType::class, $entity, array(
            'action' => $this->container->get('router')->generate('ls_contact'),
            'method' => 'PUT',
        ));
        
        
        return $this->render('LsOrderBundle:Front:index.html.twig', array(
            'form' => $form->createView(),
        ));
    }
    
    public function addAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $Order = new Order();
        $defaultKind = $em->getRepository('LsOrderBundle:OrderKind')->findOneBy(['default' => true]);
        $Order->setKind($defaultKind);
        $defaultPriority = $em->getRepository('LsOrderBundle:OrderPriority')->findOneBy(['default' => true]);
        $Order->setPriority($defaultPriority);
        $disabledDates = $em->getRepository('LsOrderBundle:DisabledDates')->findAll();
        
        $today = new \DateTime();
        
        $disabledDates = $em->createQueryBuilder()
            ->select('a')
            ->from('LsOrderBundle:DisabledDates', 'a')
            ->where('a.disabledDate is NOT NULL')
            ->andWhere('a.disabledDate >= :today')
            ->setParameter('today', $today)
            ->getQuery()
            ->getResult();
        
        $form = $this->createForm(OrderType::class, $Order, array(
            'action' => $this->container->get('router')->generate('ls_order_new'),
            'method' => 'PUT',
        ));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $files = $Order->getFiles();
            
            if ($Order->getPaymentID() == null) { 
                $creditCardId = preg_replace('/\W+/u', '', $Order->getCardId());
                $cardScheme = new CardScheme();
                $cardType = $cardScheme->getScheme($creditCardId);

                if ($cardType == false) {
                    $this->get('session')->getFlashBag()->add('error', 'Podany numer karty kredytowej jest nieprawidłowy');

                    return $this->redirect($this->generateUrl('ls_order_new'));
                }

                $paypal = new PayPal();

                $paymentResponse = $paypal->pay($Order);

                if (!$paymentResponse instanceof Payment) {
                    $this->get('session')->getFlashBag()->add('error', $paymentResponse);
                    return $this->redirect($this->generateUrl('ls_order_new'));
                }

                $status = $paymentResponse->getState(); 
                $paymentID = $paymentResponse->getId(); 

                $Order->setPaymentState($status);
                $Order->setPaymentID($paymentID);
            } else {
                $Order->setPaymentState("Zapłacono z PayPal");
            }
            
            $em->persist($Order);
            $em->flush();
            
            foreach ($files as $file) {
                $OrderFile = new File();
                $OrderFile->upload($file->getFile());
                
                $OrderFile->setOrder($Order);
                $OrderFile->setType($Order->getTypes());

                $em->persist($OrderFile);
                $em->flush($OrderFile);
            }
            
            $this->makeOrderNotification($Order);

            return $this->redirect($this->generateUrl('ls_order_ending', array(
                'id' => $Order->getId()
            )));
        } else {
            $response = new Response();
            $response->headers->clearCookie('discount');
            $response->sendHeaders();

            $session = new Session();
            $session->remove('discount');
        }
        

        return $this->render('LsOrderBundle:Front:add.html.twig', array(
            'form' => $form->createView(),
            'disabledDates' => $disabledDates
        ));
    }
    
    /**
     * Sending message about made order.
     * @param object $Order
     */
    public function makeOrderNotification($Order) 
    {
        $em = $this->getDoctrine()->getManager();
        
        $template = $em->getRepository('LsMailTemplatesBundle:MailTemplates')->findOneBy(['label' => 'new_order']);
        
        $body = $this->renderView('LsMailTemplatesBundle:Templates:newOrderTemplate.html.twig',array(
            'order' => $Order,
            'message' => $template->getValue(),
        ));
        
        $message = \Swift_Message::newInstance();
        $message->setSubject($template->getTitle());
        $message->setFrom(array($this->container->getParameter('mailer_user') => $this->container->getParameter('mailer_name')));
        $message->setTo($Order->getClientEmail());
        $message->setBody($body, 'text/html');
        $message->addPart(strip_tags($body), 'text/plain');
        
        $mailer = $this->get('mailer');
        
        return $mailer->send($message);
    }
    
    public function endingAction(Request $request, $id) 
    {
        $code = null;
        $em = $this->getDoctrine()->getManager();
        $Order = $em->getRepository('LsOrderBundle:Order')->findOneBy(array(
            'id' => $id
        ));
        
        $session = $this->container->get('session');
        $cookies = $request->cookies;
        
        if ($session->has('discount')) {
            $code = $session->get('discount');
        } else {
            if ($cookies->has('discount')) {
                $code = $cookies->get('discount');
            }
        }
        
        if ($code !== null) {
            $discountCode = $em->getRepository('LsOrderBundle:Discount')->findOneBy(['value' => $code]);
            $usesCount = $discountCode->getUsesCount();
            $usesCount = $usesCount + 1;
            $discountCode->setUsesCount($usesCount);
            $em->persist($discountCode);
            $em->flush();
            
            $response = new Response();
            $response->headers->clearCookie('discount');
            $response->sendHeaders();

            $session = new Session();
            $session->remove('discount');
        }
        
        return $this->render('LsOrderBundle:Front:ending.html.twig', array(
            'order' => $Order
        ));
    }
    
    public function checkDiscountCodeAction(Request $request) {
        $code = $request->get('_code');
        $today = new \DateTime();
        
        if (!empty($code)) {
            $em = $this->getDoctrine()->getManager();
            
            $qb = $em->createQueryBuilder();
            $discountCode = $qb->select('a')
                ->from('LsOrderBundle:Discount', 'a')
                ->where('a.value = :code')
                ->andWhere($qb->expr()->isNotNull('a.endDate'))
                ->andWhere('a.endDate >= :today')
                ->andWhere('a.usesCount < a.availableUses')
                ->setParameter('today', $today)
                ->setParameter('code', $code)
                ->getQuery()
                ->getOneOrNullResult();
            
            if (!$discountCode) {
                return new JsonResponse(['error' => true, 'message' => 'Kod jest nieprawidłowy.']);
            }
            
            $response = new Response();
            $response->headers->setCookie(new Cookie('discount', $code));
            $response->sendHeaders();
            
            $session = new Session();
            $session->set('discount', $code);
            
            return new JsonResponse(['error' => false, 'discount' => $discountCode->getDiscount()]);
        } else {
            return new JsonResponse(['error' => true, 'message' => 'Wypełnij pole!']);
        }
    }
    
    public function getPDFAction($id){
        $em = $this->getDoctrine()->getManager();
        $order = $em->getRepository('LsOrderBundle:Order')->findOneBy(array('id' => $id));
        $deliveryPrice = 0;
        $deliveryPriceDiscount = 0;
        
        $firstPagePrice = $order->getKind()->getFirstPrice();
        $nextPagePrice = $order->getKind()->getNextPrice();
        $pagesCount = $order->getPages();
        $priorityPercentage = $order->getPriority()->getPercentage();
        
        $translationPrice = $firstPagePrice + (($pagesCount-1) * $nextPagePrice);
        $translationPriceDiscount = $translationPrice*($order->getDiscount()/100);
        
        $priorityPrice = ($priorityPercentage/100) * $translationPrice;
        $priorityPriceDiscount = $priorityPrice*($order->getDiscount()/100);
        
        if ($order->getFormat() == "wydruk") {
            $deliveryPrice = $em->getRepository('LsSettingBundle:Setting')->findOneBy(array('label' => 'delivery_cost'))->getValue();
            $deliveryPriceDiscount = $deliveryPrice*($order->getDiscount()/100);
        }
        
        $total = ($priorityPrice + $translationPrice + $deliveryPrice) - ($translationPriceDiscount + $priorityPriceDiscount + $deliveryPriceDiscount);
        
        $vatPrice = $total * 0.2;
        
        $html = $this->renderView('LsOrderBundle:Front:pdfTemplate.html.twig', array(
            'order' => $order,
            'translationPrice' => $translationPrice - $translationPriceDiscount,
            'priorityPrice' => $priorityPrice - $priorityPriceDiscount,
            'deliveryPrice' => $deliveryPrice - $deliveryPriceDiscount,
            'total' => $total,
            'vatPrice' => $vatPrice
        ));

        $this->returnPDFResponseFromHTML($html);
    }
    
    public function returnPDFResponseFromHTML($html){

        $pdf = $this->get("white_october.tcpdf")->create('vertical', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->setFontSubsetting(true);
        $pdf->SetTitle('Order receipt');
        $pdf->SetMargins(15,15,15, true);
        $pdf->setFont("dejavusans");
        $pdf->AddPage();
        
        $filename = uniqid('order');
        
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 0, $fill = 0, $reseth = false, $align = '', $autopadding = false);
        $pdf->Output($filename.".pdf",'I'); // This will output the PDF as a response directly
    }
}
