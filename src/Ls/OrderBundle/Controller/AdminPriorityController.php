<?php

namespace Ls\OrderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ls\OrderBundle\Entity\OrderPriority;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Ls\OrderBundle\Form\OrderPriorityAdminType;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AdminPriorityController extends Controller {
    private $pager_limit_name = 'admin_order_priority_limit';

    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');

        $page = $request->query->get('page', 1);
        if ($session->has($this->pager_limit_name)) {
            $limit = $session->get($this->pager_limit_name);
        } else {
            $limit = 15;
            $session->set($this->pager_limit_name, $limit);
        }

        $query = $em->createQueryBuilder()
            ->select('e')
            ->from('LsOrderBundle:OrderPriority', 'e')
            ->getQuery();
        
        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $page,
            $limit,
            array(
                'defaultSortFieldName' => 'e.created_at',
                'defaultSortDirection' => 'desc',
            )
        );
        $entities->setTemplate('LsCoreBundle:Backend:paginator.html.twig');
        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl('ls_admin_order'));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Priorytet zamówienia', $this->get('router')->generate('ls_admin_order_priority'));

        return $this->render('LsOrderBundle:AdminPriority:index.html.twig', array(
            'page' => $page,
            'limit' => $limit,
            'entities' => $entities,
        ));
    }

    public function newAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = new OrderPriority();

        $form = $this->createForm(OrderPriorityAdminType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_order_priority_new'),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz i wróć na listę'));
        $form->add('submit_and_new', SubmitType::class, array('label' => 'Zapisz i dodaj następny'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            
            if ($entity->getDefault()) {
                $entities = $em->getRepository('LsOrderBundle:OrderPriority')->findAll();
                foreach ($entities as $item) {
                    $item->setDefault(false);
                    $em->persist($item);
                    $em->flush($item);
                }
                $entity->setDefault(true);
            }
            
            $em->persist($entity);
            $em->flush();

            $sitemap = $this->get('ls_core.sitemap');
            $sitemap->generate();

            $this->get('session')->getFlashBag()->add('success', 'Dodanie zakończone sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_order_priority_edit', array('id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_order_priority'));
            }
            if ($form->get('submit_and_new')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_order_priority_new'));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Priorytet zamówienia', $this->get('router')->generate('ls_admin_order_priority'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_order_priority_new'));

        return $this->render('LsOrderBundle:AdminPriority:new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsOrderBundle:OrderPriority')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find OrderPriority entity.');
        }
        
        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', 'Usunięcie zakończone sukcesem.');

        return new Response('OK');
    }
    
    public function editAction(Request $request, $id) 
    {
        $em = $this->getDoctrine()->getManager();
        if ($request->request) {
            $status = $request->request->get('form_order')['status'];
        }
        
        $Order = $em->getRepository('LsOrderBundle:OrderPriority')->find($id);
        $files = $em->createQueryBuilder()
                ->select('f')
                ->from('LsOrderBundle:File', 'f')
                ->where('f.order = :id')
                ->setParameter(':id', $id)
                ->getQuery()
                ->getResult();
        
        if (!$Order) {
            throw $this->createNotFoundException('Unable to find OrderPriority entity.');
        }
        
        $form = $this->createForm(OrderPriorityAdminType::class, $Order);
        
        $form->handleRequest($request);
        if ($form->isValid()) {
            $data = $form->getData();
            
            if ($Order->getDefault()) {
                $entities = $em->getRepository('LsOrderBundle:OrderPriority')->findAll();
                foreach ($entities as $item) {
                    $item->setDefault(false);
                    $em->persist($item);
                    $em->flush($item);
                }
                $Order->setDefault(true);
            }
            
            $em->persist($Order);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', 'Aktualizacja zakończona sukcesem.');
            
            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_order_priority_edit', array('id' => $id)));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_order_priority'));
            }
        }
        
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Wypełnij wszystkie pola.');
        }
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Priorytet zamówienia', $this->get('router')->generate('ls_admin_order'));
        $breadcrumbs->addItem($Order->__toString(), $this->get('router')->generate('ls_admin_order_priority_edit', array('id' => $Order->getId())));
        
        return $this->render('LsOrderBundle:AdminPriority:edit.html.twig', array(
            'entity' => $Order,
            'form' => $form->createView(),
            'files' => $files
        ));
    }

    public function batchAction(Request $request) {
        $ids = $request->request->get('ids');
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = 'Czy na pewno chcesz ';
            switch ($action) {
                case 'delete':
                    $message .= 'usunąć ';
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'element?';
                    break;
                case 2:
                case 3:
                case 4:
                    $message .= 'elementy?';
                    break;
                default:
                    $message .= 'elementów?';
                    break;
            }

            $breadcrumbs = $this->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
            $breadcrumbs->addItem('Priorytet zamówienia', $this->get('router')->generate('ls_admin_order_priority'));
            $breadcrumbs->addItem('Potwierdzenie', $this->get('router')->generate('ls_admin_order_priority_batch'));

            return $this->render('LsOrderBundle:AdminPriority:batch.html.twig', array(
                'message' => $message,
                'action' => $action,
                'ids' => implode(',', $ids),
            ));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_contact'));
        }
    }

    public function batchExecuteAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $ids = explode(',', $request->request->get('ids'));
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = '';
            switch ($action) {
                case 'delete':
                    $message .= 'Usunięcie ';
                    $qb = $em->createQueryBuilder();
                    $query = $qb->select('e')
                        ->from('LsOrderBundle:OrderPriority', 'e')
                        ->add('where', $qb->expr()->in('e.id', $ids))
                        ->getQuery();

                    $iterableResult = $query->iterate();
                    while (($row = $iterableResult->next()) !== false) {
                        $em->remove($row[0]);
                        $em->flush();
                    }
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'elementu ';
                    break;
                default:
                    $message .= 'elementów ';
                    break;
            }
            $message .= 'zakończone sukcesem ';

            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirect($this->generateUrl('ls_admin_order_priority'));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_order_priority'));
        }
    }

    public function setLimitAction(Request $request) {
        $session = $this->container->get('session');

        $limit = $request->request->get('limit');
        $session->set($this->pager_limit_name, $limit);

        return new Response('OK');
    }
}
