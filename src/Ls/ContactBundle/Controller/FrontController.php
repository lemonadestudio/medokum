<?php

namespace Ls\ContactBundle\Controller;

use Ls\ContactBundle\Entity\Contact;
use Ls\ContactBundle\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class FrontController extends Controller {
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = new Contact();

        $form = $this->createForm(ContactType::class, $entity, array(
            'action' => $this->container->get('router')->generate('ls_contact'),
            'method' => 'PUT',
        ));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $subject = 'Wiadomość z formularza kontaktowego';

            $message_txt = '<h3>Wiadomość z formularza kontaktowego:</h3>';
            $message_txt .= nl2br($entity->getContent()) . ' <hr />';
            $message_txt .= '<h3>Pozostałe dane:</h3>';
            $message_txt .= 'Imię i nazwisko: ' . $entity->getName() . '<br />';
            $message_txt .= 'Adres e-mail: ' . $entity->getEmail();

            $recipient_string = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('email_to_contact')->getValue();
            $recipient_array = explode(',', $recipient_string);
            $recipients = array_map('trim', $recipient_array);

            $message = \Swift_Message::newInstance();
            $message->setSubject($subject);
            $message->setFrom(array($this->container->getParameter('mailer_user') => $this->container->getParameter('mailer_name')));
            $message->setTo($recipients);
            $message->setBody($message_txt, 'text/html');
            $message->addPart(strip_tags($message_txt), 'text/plain');

            $mailer = $this->get('mailer');
            $mailer->send($message);

            $this->get('session')->getFlashBag()->add('success', 'Twoja wiadomość została wysłana.');

            return $this->redirect($this->generateUrl('ls_contact'));
        }

        return $this->render('LsContactBundle:Front:index.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
