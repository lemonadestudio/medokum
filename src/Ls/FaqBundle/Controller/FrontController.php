<?php

namespace Ls\FaqBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Page controller.
 *
 */
class FrontController extends Controller {

    /**
     * Finds and displays a Faq entity.
     *
     */
    public function ListAction() {
        $em = $this->getDoctrine()->getManager();
        
        $faqCategories = $em->createQueryBuilder()
                ->select('c')
                ->from('LsFaqBundle:FaqCategories', 'c')
                ->getQuery()
                ->getResult();

        $faqs = $em->createQueryBuilder()
            ->select('f')
            ->from('LsFaqBundle:Faq', 'f')
            ->getQuery()
            ->getResult();

        if (null === $faqs) {
            throw $this->createNotFoundException('Unable to find Faq entity.');
        }

        return $this->render('LsFaqBundle:Front:list.html.twig', array(
            'categories' => $faqCategories,
            'faqs' => $faqs,
        ));
    }

}
