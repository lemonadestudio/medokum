<?php

namespace Ls\OrderBundle\Service;

use Knp\Menu\ItemInterface;
use Ls\CoreBundle\Helper\AdminBlock;
use Ls\CoreBundle\Helper\AdminLink;
use Ls\CoreBundle\Helper\AdminRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AdminService {
    private $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    public function addToMenu(ItemInterface $parent, $route, $set) {
        $item = $parent->addChild('Zobacz zamówienia', array(
            'route' => 'ls_admin_order',
        ));
        
        $current_set = true;

        if (!$set) {
            switch ($route) {
                case 'ls_admin_order':
                case 'ls_admin_order_show':
                case 'ls_admin_order_batch':
                    $item->setCurrent(true);
                    break;
                default:
                    $current_set = false;
                    break;
            }
        }
        
        $item = $parent->addChild('Rodzaje zamówień', array(
            'route' => 'ls_admin_order_kind',
        ));
        
        $current_set = true;

        if (!$set) {
            switch ($route) {
                case 'ls_admin_order_kind':
                case 'ls_admin_order_kind_show':
                case 'ls_admin_order_kind_new':
                case 'ls_admin_order_kind_edit':
                case 'ls_admin_order_kind_batch':
                    $item->setCurrent(true);
                    break;
                default:
                    $current_set = false;
                    break;
            }
        }
        
        $item = $parent->addChild('Typy dokumentów', array(
            'route' => 'ls_admin_file_type',
        ));
        
        $current_set = true;

        if (!$set) {
            switch ($route) {
                case 'ls_admin_file_type':
                case 'ls_admin_file_type_show':
                case 'ls_admin_file_type_new':
                case 'ls_admin_file_type_edit':
                case 'ls_admin_file_type_batch':
                    $item->setCurrent(true);
                    break;
                default:
                    $current_set = false;
                    break;
            }
        }
        
        $item = $parent->addChild('Priorytet zamówienia', array(
            'route' => 'ls_admin_order_priority',
        ));
        
        $current_set = true;

        if (!$set) {
            switch ($route) {
                case 'ls_admin_order_priority':
                case 'ls_admin_order_priority_show':
                case 'ls_admin_order_priority_new':
                case 'ls_admin_order_priority_edit':
                case 'ls_admin_order_priority_batch':
                    $item->setCurrent(true);
                    break;
                default:
                    $current_set = false;
                    break;
            }
        }
        
        $item = $parent->addChild('Święta i dni wolne', array(
            'route' => 'ls_admin_order_priority',
        ));
        
        $current_set = true;

        if (!$set) {
            switch ($route) {
                case 'ls_admin_order_disabled_dates':
                case 'ls_admin_order_disabled_dates_show':
                case 'ls_admin_order_disabled_dates_new':
                case 'ls_admin_order_disabled_dates_edit':
                case 'ls_admin_order_disabled_dates_batch':
                    $item->setCurrent(true);
                    break;
                default:
                    $current_set = false;
                    break;
            }
        }

        return $current_set;
    }

    public function addToDashboard(AdminBlock $parent) {
        $router = $this->container->get('router');

        $row = new AdminRow('Zarządzanie zamówieniami');
        $parent->addRow($row);

        $row->addLink(new AdminLink('Lista zamówień', 'glyphicon-list', $router->generate('ls_admin_order')));
        
        $row = new AdminRow('Rodzaje zamówień');
        $parent->addRow($row);

        $row->addLink(new AdminLink('Dodaj', 'glyphicon-plus', $router->generate('ls_admin_order_kind_new')));
        $row->addLink(new AdminLink('Zarządzaj', 'glyphicon-list', $router->generate('ls_admin_order_kind')));
        
        $row = new AdminRow('Typy dokumentów');
        $parent->addRow($row);

        $row->addLink(new AdminLink('Dodaj', 'glyphicon-list', $router->generate('ls_admin_file_type_new')));
        $row->addLink(new AdminLink('Zarządzaj', 'glyphicon-list', $router->generate('ls_admin_file_type')));
        
        $row = new AdminRow('Priorytet zamówienia');
        $parent->addRow($row);

        $row->addLink(new AdminLink('Dodaj', 'glyphicon-list', $router->generate('ls_admin_order_priority_new')));
        $row->addLink(new AdminLink('Zarządzaj', 'glyphicon-list', $router->generate('ls_admin_order_priority')));
        
        $row = new AdminRow('Święta i dni wolne');
        $parent->addRow($row);

        $row->addLink(new AdminLink('Dodaj', 'glyphicon-list', $router->generate('ls_admin_order_disabled_dates_new')));
        $row->addLink(new AdminLink('Zarządzaj', 'glyphicon-list', $router->generate('ls_admin_order_disabled_dates')));
    }
}

