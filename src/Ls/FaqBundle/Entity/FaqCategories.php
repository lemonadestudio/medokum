<?php

namespace Ls\FaqBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Ls\GalleryBundle\Entity\HasGallery;

/**
 * FaqCategories
 * @ORM\Table(name="faq_categories")
 * @ORM\Entity
 */
class FaqCategories
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $seo_generate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_keywords;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_description;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updated_at;
    
    /**
     * @ORM\OneToMany(targetEntity="Faq", mappedBy="category")
     */
    private $faq;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->seo_generate = true;
        $this->created_at = new \DateTime();
    }

    public function __toString()
    {
        if (is_null($this->getTitle())) {
            return 'NULL';
        }

        return $this->getTitle();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return FaqCategories
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return FaqCategories
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    
        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set seoGenerate
     *
     * @param boolean $seoGenerate
     *
     * @return FaqCategories
     */
    public function setSeoGenerate($seoGenerate)
    {
        $this->seo_generate = $seoGenerate;
    
        return $this;
    }

    /**
     * Get seoGenerate
     *
     * @return boolean
     */
    public function getSeoGenerate()
    {
        return $this->seo_generate;
    }

    /**
     * Set seoTitle
     *
     * @param string $seoTitle
     *
     * @return FaqCategories
     */
    public function setSeoTitle($seoTitle)
    {
        $this->seo_title = $seoTitle;
    
        return $this;
    }

    /**
     * Get seoTitle
     *
     * @return string
     */
    public function getSeoTitle()
    {
        return $this->seo_title;
    }

    /**
     * Set seoKeywords
     *
     * @param string $seoKeywords
     *
     * @return FaqCategories
     */
    public function setSeoKeywords($seoKeywords)
    {
        $this->seo_keywords = $seoKeywords;
    
        return $this;
    }

    /**
     * Get seoKeywords
     *
     * @return string
     */
    public function getSeoKeywords()
    {
        return $this->seo_keywords;
    }

    /**
     * Set seoDescription
     *
     * @param string $seoDescription
     *
     * @return FaqCategories
     */
    public function setSeoDescription($seoDescription)
    {
        $this->seo_description = $seoDescription;
    
        return $this;
    }

    /**
     * Get seoDescription
     *
     * @return string
     */
    public function getSeoDescription()
    {
        return $this->seo_description;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return FaqCategories
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return FaqCategories
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Add faq
     *
     * @param \Ls\FaqBundle\Entity\Faq $faq
     *
     * @return FaqCategories
     */
    public function addFaq(\Ls\FaqBundle\Entity\Faq $faq)
    {
        $this->faq[] = $faq;
    
        return $this;
    }

    /**
     * Remove faq
     *
     * @param \Ls\FaqBundle\Entity\Faq $faq
     */
    public function removeFaq(\Ls\FaqBundle\Entity\Faq $faq)
    {
        $this->faq->removeElement($faq);
    }

    /**
     * Get faq
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFaq()
    {
        return $this->faq;
    }
}
