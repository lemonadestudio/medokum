<?php

namespace Ls\GalleryBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class HasGalleryType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('gallery', EntityType::class, array(
            'label' => 'Galeria',
            'class' => 'LsGalleryBundle:Gallery',
            'required' => false,
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('g')
                    ->where('g.attachable = 1');
            }
        ));
    }
}
