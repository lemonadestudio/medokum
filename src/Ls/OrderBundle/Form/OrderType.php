<?php

namespace Ls\OrderBundle\Form;

use Ls\OrderBundle\Entity\FileType as File;
use Ls\OrderBundle\Entity\OrderKind as OrderKind;
use Ls\OrderBundle\Entity\OrderPriority as OrderPriority;
use Symfony\Component\Form\AbstractType;
use Ls\OrderBundle\Form\OrderFileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Ls\CoreBundle\Form\DataTransformer\DateTimeTransformer;

class OrderType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('files', CollectionType::class, array(
                'label_attr' => array(
                    'style' => 'display: none;'
                ),
                'entry_type' => OrderFileType::class,
                'mapped' => true,
                'allow_add' => true,
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Wypełnij pole'))
                )
            )
        );
        $builder->add('types', EntityType::class, array(
                'label' => 'Typ dokumentu:',
                'class' => 'LsOrderBundle:FileType',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array(
                        'message' => 'Wypełnij pole'
                    )),
                )
            )
        );
        $builder->add('pages', IntegerType::class, array(
                'label' => 'Ilość stron:',
                'attr' => array(
                    'value' => '1',
                    'data-price' => '1',
                    'min' => '1',
                    'class' => 'pages-count'
                ),
                'required' => true,
                'constraints' => array(
                    new NotBlank(array(
                        'message' => 'Wypełnij pole'
                    )),
                )
            )
        );
        
        $builder->add('kind', EntityType::class, array(
                'class' => 'LsOrderBundle:OrderKind',
                'expanded' => true,
                'multiple' => false,
                'required' => true,
                'choice_attr' => function(OrderKind $val, $key) { 
                    return [
                        'data-firstPrice' => $val->getFirstPrice(),
                        'data-nextPrice' => $val->getNextPrice(),
                        ]; 
                },
                'constraints' => array(
                    new NotBlank(array(
                        'message' => 'Wypełnij pole'
                    )),
                )
        )); 
                
        $builder->add('finish_date', TextType::class, array(
            'required' => true,
            'attr' => array(
                'data-date-format' => 'DD.MM.YYYY',
                'placeholder' => "--"
            ),
        ));
        
        $builder->get('finish_date')->addModelTransformer(new DateTimeTransformer());
        
        $builder->add('priority', EntityType::class, array(
                'class' => 'LsOrderBundle:OrderPriority',
                'expanded' => true,
                'multiple' => false,
                'required' => true,
                'choice_attr' => function(OrderPriority $val, $key) { 
                    return [
                        'data-percentage' => $val->getPercentage(),
                        'data-startDay' => $val->getStartDay(),
                        'data-endDay' => $val->getEndDay()
                    ]; 
                },
                'constraints' => array(
                    new NotBlank(array(
                        'message' => 'Wypełnij pole'
                    )),
                )
        ));
        
        $builder->add('format', ChoiceType::class, array(
                'choices' => array(
                    'Format PDF' => 'pdf',
                    'Wydruk' => 'wydruk'
                ),
                'data' => 'pdf',
                'expanded' => true,
                'constraints' => array(
                    new NotBlank(array(
                        'message' => 'Wypełnij pole'
                    )),
                )
        )); 
        
        $builder->add('name', null, array(
                'label' => 'Imię i nazwisko:',
                'required' => true,
        )); 
        
        $builder->add('client_email', null, array(
                'label' => 'Adres email:',
                'required' => true,
        )); 
        
        $builder->add('address', null, array(
                'label' => 'Adres:',
                'required' => true,
        )); 
        
        $builder->add('price', HiddenType::class, array(
        )); 
        
        $builder->add('endPrice', HiddenType::class, array(
        )); 
        
        $builder->add('discount', HiddenType::class, array(
        )); 
        
        $builder->add('paymentID', HiddenType::class, array(
        )); 
        
        $builder->add('payerID', HiddenType::class, array(
        )); 
        
        $builder->add('owner_name', null, array(
            'label' => 'Imię i nazwisko:',
            'required' => true
        )); 
        
        $builder->add('card_id', TextType::class, array(
            'label' => 'Numer karty:',
            'required' => true
        )); 

        $builder->add('expiration', DateType::class, array(
            'label' => 'Data ważności',
            'required' => true,
            'format' => 'dMMy',
            'years' => range(date('y'), date('y')+12),
            'days' => array(1),
        )); 

        $builder->add('ccv', IntegerType::class, array(
            'label' => 'CVV2 lub CVC2:',
            'required' => true,
            'attr' => array(
                'max' => '9999',
                'min' => '100'
            )
        )); 
        
//        $builder->add('submit', SubmitType::class, array(
//                'label' => 'Kontynuuj'
//            )
//        );
//        $builder->add('submit_and_list', SubmitType::class, array(
//            'label' => 'Wróć'
//        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\OrderBundle\Entity\Order'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_order';
    }
}
