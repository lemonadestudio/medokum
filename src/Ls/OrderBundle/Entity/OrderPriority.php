<?php

namespace Ls\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * OrderPriority
 * @ORM\Table(name="orders_priority")
 * @ORM\Entity
 */
class OrderPriority {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $name;
    
    /**
     * @ORM\Column(type="decimal", scale=2)
     * @var decimal
     */
    private $percentage;

    /**
     * @ORM\Column(name="defaultValue", type="boolean", nullable=true)
     * @var string
     */
    private $default;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var string
     */
    private $startDay;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var string
     */
    private $endDay;
    
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * Constructor
     */
    public function __construct() {
        $this->created_at = new \DateTime();
        $this->default = false;
    }

    public function __toString()
    {
        if (is_null($this->getName())) {
            return 'NULL';
        }

        return $this->getName();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return OrderPriority
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set percentage
     *
     * @param integer $percentage
     *
     * @return OrderPriority
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;
    
        return $this;
    }

    /**
     * Get percentage
     *
     * @return integer
     */
    public function getPercentage()
    {
        return $this->percentage;
    }
    
    /**
     * Set default
     *
     * @param integer $default
     *
     * @return OrderPriority
     */
    public function setDefault($default)
    {
        $this->default = $default;
    
        return $this;
    }

    /**
     * Get default
     *
     * @return integer
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return OrderPriority
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    
    /**
     * Set startDay
     *
     * @param integer $startDay
     *
     * @return OrderPriority
     */
    public function setStartDay($startDay)
    {
        $this->startDay = $startDay;
    
        return $this;
    }

    /**
     * Get startDay
     *
     * @return integer
     */
    public function getStartDay()
    {
        return $this->startDay;
    }
    
    /**
     * Set endDay
     *
     * @param integer $endDay
     *
     * @return OrderPriority
     */
    public function setEndDay($endDay)
    {
        $this->endDay = $endDay;
    
        return $this;
    }

    /**
     * Get endDay
     *
     * @return integer
     */
    public function getEndDay()
    {
        return $this->endDay;
    }
}
