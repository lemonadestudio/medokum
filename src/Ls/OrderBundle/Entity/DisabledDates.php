<?php

namespace Ls\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * DisabledDates
 * @ORM\Table(name="disabled_dates")
 * @ORM\Entity
 */
class DisabledDates {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $disabledDate;

    /**
     * Constructor
     */
    public function __construct() {
        $this->disabledDate = new \DateTime();
    }

    public function __toString()
    {
        if (is_null($this->id)) {
            return 'NULL';
        }

        return $this->disabledDate->format('d.m.Y');
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set disabledDate
     *
     * @param \DateTime $disabledDate
     *
     * @return DisabledDates
     */
    public function setDisabledDate($disabledDate)
    {
        $this->disabledDate = $disabledDate;
    
        return $this;
    }

    /**
     * Get disabledDate
     *
     * @return \DateTime
     */
    public function getDisabledDate()
    {
        return $this->disabledDate;
    }
}
