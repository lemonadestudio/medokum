$(function () {
    fckconfig_common = {
        skin: 'BootstrapCK-Skin',
        contentsCss: ['/bundles/lscore/front/css/editor.css'],
        bodyClass: 'editor',
        stylesSet: [
            {name: 'Normalny', element: 'p', attributes: {'class': ''}},
            {name: 'Nagłówek', element: 'p.naglowek, h1', attributes: {'class': 'naglowek'}}
        ],
        filebrowserBrowseUrl: kcfinderBrowseUrl + '?type=files',
        filebrowserImageBrowseUrl: kcfinderBrowseUrl + '?type=images',
        filebrowserFlashBrowseUrl: kcfinderBrowseUrl + '?type=flash',
        filebrowserUploadUrl: '/bundles/lscore/common/kcfinder-3.20/upload.php?type=files',
        filebrowserImageUploadUrl: '/bundles/lscore/common/kcfinder-3.20/upload.php?type=images',
        filebrowserFlashUploadUrl: '/bundles/lscore/common/kcfinder-3.20/upload.php?type=flash'
    };

    fckconfig = jQuery.extend(true, {
        height: '400px',
        width: 'auto'
    }, fckconfig_common);

    $('.wysiwyg').ckeditor(fckconfig);
    
    fckconfig_common2 = {
        skin: 'BootstrapCK-Skin',
        contentsCss: ['/bundles/lscore/front/css/editor.css'],
        bodyClass: 'editor',
        filebrowserBrowseUrl: kcfinderBrowseUrl + '?type=files',
        filebrowserImageBrowseUrl: kcfinderBrowseUrl + '?type=images',
        filebrowserFlashBrowseUrl: kcfinderBrowseUrl + '?type=flash',
        filebrowserUploadUrl: '/bundles/lscore/common/kcfinder-3.20/upload.php?type=files',
        filebrowserImageUploadUrl: '/bundles/lscore/common/kcfinder-3.20/upload.php?type=images',
        filebrowserFlashUploadUrl: '/bundles/lscore/common/kcfinder-3.20/upload.php?type=flash'
    };

    fckconfig2 = jQuery.extend(true, {
        height: '400px',
        width: 'auto'
    }, fckconfig_common2);

    $('.wysiwyg2').ckeditor(fckconfig2);
});