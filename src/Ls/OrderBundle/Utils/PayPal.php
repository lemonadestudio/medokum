<?php

namespace Ls\OrderBundle\Utils;

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\FundingInstrument;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentCard;
use PayPal\Api\Transaction;
use PayPal\Exception\PayPalConnectionException;
use Ls\OrderBundle\Entity\Order;
use Ls\CoreBundle\Utils\CardScheme;

class PayPal {
    public function pay(Order $order) {
        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                'AVDz2rY6ySODdoObC81VZmPRaCmnJVuRDVZ-UCpu2af1df24dg7QIlQxn4AWu9BMAXqxViB8Hy_whr8f',     // ClientID
                'EHdOu5640AxIdRGMJp5XD6DXMnfgsT29iSwUdBgfw5LBIEz59Aoc_5NCPejtxBN-HV1sf50vGbeUNJg5'      // ClientSecret
            )
        );
        
        $creditCardId = preg_replace('/\W+/u', '', $order->getCardId());
        $cardScheme = new CardScheme();
        $cardType = $cardScheme->getScheme($creditCardId);
        $expire = $order->getExpiration();     
        $name = $this->split_name($order->getOwnerName());
        
        
        $card = new PaymentCard();
        $card->setType($cardType)
            ->setNumber($creditCardId)
            ->setExpireMonth($expire->format('m'))
            ->setExpireYear($expire->format('Y'))
            ->setCvv2($order->getCcv())
            ->setFirstName($name['first_name'])
            ->setBillingCountry("GB")
            ->setLastName($name['last_name']);
        
        $fi = new FundingInstrument();
        $fi->setPaymentCard($card);
        
        $payer = new Payer();
        $payer->setPaymentMethod("credit_card")
            ->setFundingInstruments(array($fi));
        
        $item1 = new Item();
        $item1->setName('Tłumaczenie')
            ->setDescription('Tłumaczenie dokumentacji medycznej')
            ->setCurrency('GBP')
            ->setQuantity(1)
            ->setTax(0)
            ->setPrice($order->getEndPrice());
        
        $itemList = new ItemList();
        $itemList->setItems(array($item1));
        
        $details = new Details();
        $details->setShipping(0)
            ->setTax(0)
            ->setSubtotal($order->getEndPrice());
        
        $amount = new Amount();
        $amount->setCurrency("GBP")
            ->setTotal($order->getEndPrice())
            ->setDetails($details);
        
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Tłumaczenie dokumentacji medycznej")
            ->setInvoiceNumber(uniqid());
        
        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setTransactions(array($transaction));
        
        $request = clone $payment;
        
        try {
            $payment->create($apiContext);
        }catch(PayPalConnectionException $e){
            $errorData = json_decode($e->getData());
            if (isset($errorData->name)) {
                if ($errorData->name == "VALIDATION_ERROR") {
                    return "Błąd walidacji danych. Podaj poprawne dane.";
                } else {
                    return "Najprawdopodobniej wystąpił błąd inicjowania lub aktualizacji transakcji. Odczekaj chwilę i spróbuj ponownie lub skontaktuj się z administratorem.";
                }
            } 
            
            return "Najprawdopodobniej wystąpił błąd inicjowania lub aktualizacji transakcji. Odczekaj chwilę i spróbuj ponownie lub skontaktuj się z administratorem.";
        } catch (Exception $ex) {
            return "Najprawdopodobniej wystąpił błąd inicjowania lub aktualizacji transakcji. Odczekaj chwilę i spróbuj ponownie lub skontaktuj się z administratorem.";
        }
        
        return $payment;
    }
    
    private function split_name($name) {
        $name = trim($name);
        $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );
        return array('first_name' => $first_name, 'last_name' => $last_name);
    }
}