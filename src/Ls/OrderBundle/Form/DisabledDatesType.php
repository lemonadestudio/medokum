<?php

namespace Ls\OrderBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Ls\CoreBundle\Form\DataTransformer\DateTimeTransformer;

class DisabledDatesType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('disabledDate', TextType::class, array(
            'label' => 'Wybierz dzień',
            'attr' => array(
                'data-date-format' => 'DD.MM.YYYY'
            ),
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->get('disabledDate')->addModelTransformer(new DateTimeTransformer());
        
        $builder->add('submit', SubmitType::class, array(
                'label' => 'Zapisz'
            )
        );
        $builder->add('submit_and_list', SubmitType::class, array(
            'label' => 'Zapisz i przejdź do listy'
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array());
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_order_priority';
    }
}
