<?php

namespace Ls\OrderBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

class MailHandlerService {
    private $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }
    
    public function statusChangeMessageSend ($Order) {
        $Message = \Swift_Message::newInstance();
        
        $Message->setSubject('Zmiana statusu zamówienia');
        $Message->setTo($Order->getClientEmail());
        $Message->setFrom('tomek@lemonadestudio.pl', 'Portal Medikum.co.uk');
        $Message->setBody('Status twojego zamówienia '.$Order->getName(). ' został zmieniony na: '.$Order->getStatus());
        
        $Mailer = $this->container->get('mailer');
        $Mailer->send($Message);
        
    }
}