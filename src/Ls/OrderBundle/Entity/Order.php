<?php

namespace Ls\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\CardSchemeValidator;

/**
 * Order
 * @ORM\Table(name="orders")
 * @ORM\Entity
 */
class Order {
    

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    protected $types;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $clientEmail;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $address;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $content;
    
    /**
     * @ORM\Column(type="text")
     * @var string
     */
    private $status;
    
    /**
     * @ORM\Column(type="text")
     * @var string
     */
    private $format;
    
    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $pages;
    
    /**
     * @ORM\ManyToOne(targetEntity="OrderPriority")
     * @ORM\JoinColumn(name="priority_id", referencedColumnName="id")
     */
    private $priority;
    
    /**
     * @ORM\ManyToOne(targetEntity="OrderKind")
     * @ORM\JoinColumn(name="kind_id", referencedColumnName="id")
     */
    private $kind;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $finish_date;
    
    /**
     *  @ORM\OneToMany(
     *   targetEntity="File",
     *   mappedBy="order",
     *   cascade={"all"}
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $files;
    
    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     * @var integer
     */
    private $price;
    
    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     * @var integer
     */
    private $endPrice;
    
    /**
     * @ORM\Column(type="string", length=254, nullable=true)
     * @var integer
     */
    private $discount;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     * @var integer
     */
    private $paymentID;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     * @var integer
     */
    private $paymentState;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     * @var integer
     */
    private $payerID;
    
    private $card_id;
    private $expiration;
    private $ccv;
    private $owner_name;
    
    /**
     * Constructor
     */
    public function __construct() {
        $this->created_at = new \DateTime();
        $this->status = 'Przyjęte';
        $this->files = new ArrayCollection();
        $this->types = new ArrayCollection();
        $this->discount = 0;
    }

    public function __toString()
    {
        if (is_null($this->getName())) {
            return 'NULL';
        }

        return $this->getName();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set types
     *
     * @param string $types
     *
     * @return Order
     */
    public function setTypes($types)
    {
        $this->types = $types;
    
        return $this;
    }

    /**
     * Get types
     *
     * @return string
     */
    public function getTypes()
    {
        return $this->types;
    }
    
    /**
     * Set paymentID
     *
     * @param string $paymentID
     *
     * @return Order
     */
    public function setPaymentID($paymentID)
    {
        $this->paymentID = $paymentID;
    
        return $this;
    }

    /**
     * Get paymentID
     *
     * @return string
     */
    public function getPaymentID()
    {
        return $this->paymentID;
    }
    
    
    /**
     * Set paymentState
     *
     * @param string $paymentState
     *
     * @return Order
     */
    public function setPaymentState($paymentState)
    {
        $this->paymentState = $paymentState;
    
        return $this;
    }

    /**
     * Get paymentState
     *
     * @return string
     */
    public function getPaymentState()
    {
        return $this->paymentState;
    }
    
    /**
     * Set payerID
     *
     * @param string $payerID
     *
     * @return Order
     */
    public function setPayerID($payerID)
    {
        $this->payerID = $payerID;
    
        return $this;
    }

    /**
     * Get payerID
     *
     * @return string
     */
    public function getPayerID()
    {
        return $this->payerID;
    }
    
    /**
     * Set price
     *
     * @param string $price
     *
     * @return Order
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }
    
    /**
     * Set endPrice
     *
     * @param string $endPrice
     *
     * @return Order
     */
    public function setEndPrice($endPrice)
    {
        $this->endPrice = $endPrice;
    
        return $this;
    }

    /**
     * Get endPrice
     *
     * @return string
     */
    public function getEndPrice()
    {
        return $this->endPrice;
    }
    
    /**
     * Set discount
     *
     * @param string $discount
     *
     * @return Order
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    
        return $this;
    }

    /**
     * Get discount
     *
     * @return string
     */
    public function getDiscount()
    {
        return $this->discount;
    }


    /**
     * Set name
     *
     * @param string $name
     *
     * @return Order
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set clientEmail
     *
     * @param string $clientEmail
     *
     * @return Order
     */
    public function setClientEmail($clientEmail)
    {
        $this->clientEmail = $clientEmail;
    
        return $this;
    }

    /**
     * Get clientEmail
     *
     * @return string
     */
    public function getClientEmail()
    {
        return $this->clientEmail;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Order
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Order
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Order
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set finishDate
     *
     * @param \DateTime $finishDate
     *
     * @return Order
     */
    public function setFinishDate($finishDate)
    {
        $this->finish_date = $finishDate;
    
        return $this;
    }

    /**
     * Get finishDate
     *
     * @return \DateTime
     */
    public function getFinishDate()
    {
        return $this->finish_date;
    }

    /**
     * Set kind
     *
     * @param \Ls\OrderBundle\Entity\OrderKind $kind
     *
     * @return Order
     */
    public function setKind(\Ls\OrderBundle\Entity\OrderKind $kind = null)
    {
        $this->kind = $kind;
    
        return $this;
    }

    /**
     * Get kind
     *
     * @return \Ls\OrderBundle\Entity\OrderKind
     */
    public function getKind()
    {
        return $this->kind;
    }

    /**
     * Add file
     *
     * @param \Ls\OrderBundle\Entity\File $file
     *
     * @return Order
     */
    public function addFile(\Ls\OrderBundle\Entity\File $file)
    {
        $this->files[] = $file;
    
        return $this;
    }

    /**
     * Remove file
     *
     * @param \Ls\OrderBundle\Entity\File $file
     */
    public function removeFile(\Ls\OrderBundle\Entity\File $file)
    {
        $this->files->removeElement($file);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set priority
     *
     * @param \Ls\OrderBundle\Entity\OrderPriority $priority
     *
     * @return Order
     */
    public function setPriority(\Ls\OrderBundle\Entity\OrderPriority $priority = null)
    {
        $this->priority = $priority;
    
        return $this;
    }

    /**
     * Get priority
     *
     * @return \Ls\OrderBundle\Entity\OrderPriority
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set format
     *
     * @param string $format
     *
     * @return Order
     */
    public function setFormat($format)
    {
        $this->format = $format;
    
        return $this;
    }

    /**
     * Get format
     *
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }
    
    /**
     * Get formatName
     *
     * @return string
     */
    public function getFormatName($format)
    {
        $formats = [
            'pdf' => 'Format PDF',
            'wydruk' => 'Wydruk'
        ];
        
        return $formats[$format];
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Order
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set pages
     *
     * @param integer $pages
     *
     * @return Order
     */
    public function setPages($pages)
    {
        $this->pages = $pages;
    
        return $this;
    }

    /**
     * Get pages
     *
     * @return integer
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * Set card_id
     *
     * @param integer $card_id
     *
     * @return Order
     */
    public function setCardId($card_id)
    {
        $this->card_id = $card_id;
    
        return $this;
    }

    /**
     * Get card_id
     *
     * @return integer
     */
    public function getCardId()
    {
        return $this->card_id;
    }

    /**
     * Set owner_name
     *
     * @param integer $owner_name
     *
     * @return Order
     */
    public function setOwnerName($owner_name)
    {
        $this->owner_name = $owner_name;
    
        return $this;
    }

    /**
     * Get owner_name
     *
     * @return integer
     */
    public function getOwnerName()
    {
        return $this->owner_name;
    }

    /**
     * Set expiration
     *
     * @param integer $expiration
     *
     * @return Order
     */
    public function setExpiration($expiration)
    {
        $this->expiration = $expiration;
    
        return $this;
    }

    /**
     * Get expiration
     *
     * @return integer
     */
    public function getExpiration()
    {
        return $this->expiration;
    }

    /**
     * Set ccv
     *
     * @param integer $ccv
     *
     * @return Order
     */
    public function setCcv($ccv)
    {
        $this->ccv = $ccv;
    
        return $this;
    }

    /**
     * Get ccv
     *
     * @return integer
     */
    public function getCcv()
    {
        return $this->ccv;
    }
}
