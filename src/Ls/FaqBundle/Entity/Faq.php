<?php

namespace Ls\FaqBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Ls\GalleryBundle\Entity\HasGallery;

/**
 * Faq
 * @ORM\Table(name="faq")
 * @ORM\Entity
 */
class Faq
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $content_short_generate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $content_short;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $content;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $seo_generate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_keywords;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_description;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updated_at;
    
    /**
     * @ORM\ManyToOne(targetEntity="FaqCategories", inversedBy="faq")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->content_short_generate = true;
        $this->seo_generate = true;
        $this->created_at = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Faq
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Faq
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    
        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set contentShortGenerate
     *
     * @param boolean $contentShortGenerate
     *
     * @return Faq
     */
    public function setContentShortGenerate($contentShortGenerate)
    {
        $this->content_short_generate = $contentShortGenerate;
    
        return $this;
    }

    /**
     * Get contentShortGenerate
     *
     * @return boolean
     */
    public function getContentShortGenerate()
    {
        return $this->content_short_generate;
    }

    /**
     * Set contentShort
     *
     * @param string $contentShort
     *
     * @return Faq
     */
    public function setContentShort($contentShort)
    {
        $this->content_short = $contentShort;
    
        return $this;
    }

    /**
     * Get contentShort
     *
     * @return string
     */
    public function getContentShort()
    {
        return $this->content_short;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Faq
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set seoGenerate
     *
     * @param boolean $seoGenerate
     *
     * @return Faq
     */
    public function setSeoGenerate($seoGenerate)
    {
        $this->seo_generate = $seoGenerate;
    
        return $this;
    }

    /**
     * Get seoGenerate
     *
     * @return boolean
     */
    public function getSeoGenerate()
    {
        return $this->seo_generate;
    }

    /**
     * Set seoTitle
     *
     * @param string $seoTitle
     *
     * @return Faq
     */
    public function setSeoTitle($seoTitle)
    {
        $this->seo_title = $seoTitle;
    
        return $this;
    }

    /**
     * Get seoTitle
     *
     * @return string
     */
    public function getSeoTitle()
    {
        return $this->seo_title;
    }

    /**
     * Set seoKeywords
     *
     * @param string $seoKeywords
     *
     * @return Faq
     */
    public function setSeoKeywords($seoKeywords)
    {
        $this->seo_keywords = $seoKeywords;
    
        return $this;
    }

    /**
     * Get seoKeywords
     *
     * @return string
     */
    public function getSeoKeywords()
    {
        return $this->seo_keywords;
    }

    /**
     * Set seoDescription
     *
     * @param string $seoDescription
     *
     * @return Faq
     */
    public function setSeoDescription($seoDescription)
    {
        $this->seo_description = $seoDescription;
    
        return $this;
    }

    /**
     * Get seoDescription
     *
     * @return string
     */
    public function getSeoDescription()
    {
        return $this->seo_description;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Faq
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Faq
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function __toString()
    {
        if (is_null($this->getTitle())) {
            return 'NULL';
        }

        return $this->getTitle();
    }

    /**
     * Set category
     *
     * @param \Ls\FaqBundle\Entity\FaqCategories $category
     *
     * @return Faq
     */
    public function setCategory(\Ls\FaqBundle\Entity\FaqCategories $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \Ls\FaqBundle\Entity\FaqCategories
     */
    public function getCategory()
    {
        return $this->category;
    }
}
