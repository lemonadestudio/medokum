<?php

namespace Ls\IndexContentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class IndexContentType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $object = $event->getData();
            $form = $event->getForm();
            
            if (!$object || null === $object->getId()) {
                $form->add('label', null, array(
                    'label' => 'Etykieta',
                    'constraints' => array(
                        new NotBlank(array(
                            'message' => 'Wypełnij pole'
                        ))
                    )
                ));
            } else {
                $form->add('label', null, array(
                    'label' => 'Etykieta',
                    'disabled' => true,
                    'constraints' => array(
                        new NotBlank(array(
                            'message' => 'Wypełnij pole'
                        ))
                    )
                ));
            }
        });
        
        $builder->add('description', null, array(
            'label' => 'Opis',
        ));
        $builder->add('value', null, array(
            'label' => 'Wartość',
            'attr' => array(
                'rows' => 5
            )
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\IndexContentBundle\Entity\IndexContent',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_admin_index_content';
    }
}
