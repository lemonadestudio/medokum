<?php

namespace Ls\OrderBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class OrderKindAdminType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('name', null, array(
                'label' => 'Nazwa',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Wypełnij pole'))
                )
            )
        );
        
        $builder->add('default', null, array(
            'label' => 'Domyślnie zaznaczony'
        ));
        
        $builder->add('firstPrice', null, array(
                'label' => 'Cena za pierwszą stronę',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Wypełnij pole'))
                )
            )
        );
        $builder->add('nextPrice', null, array(
                'label' => 'Cena za każdą kolejną stronę',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Wypełnij pole'))
                )
            )
        );
        $builder->add('content', TextareaType::class, array(
                'label' => 'Opis:',
                'required' => false,
            )
        );
        $builder->add('submit', SubmitType::class, array(
                'label' => 'Zapisz'
            )
        );
        $builder->add('submit_and_list', SubmitType::class, array(
            'label' => 'Zapisz i przejdź do listy'
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array());
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_order_kind';
    }
}
