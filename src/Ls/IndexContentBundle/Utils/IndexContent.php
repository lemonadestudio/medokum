<?php

namespace Ls\IndexContentBundle\Utils;

use Doctrine\ORM\EntityManager;
use Ls\IndexContentBundle\Entity\IndexContent as IndexContentEntity;
use Symfony\Component\DependencyInjection\ContainerInterface;

class IndexContent {

    protected $em;
    protected $repo;
    protected $container;
    protected $index_content  = array();
    protected $is_loaded = false;

    public function __construct(EntityManager $em, ContainerInterface $container) {
        $this->em = $em;
        $this->container = $container;
    }

    public function get($name, $default) {
        if (array_key_exists($name, $this->index_content)) {
            return $this->index_content[$name];
        }
        return $default;
    }

    public function all() {
        $index_content = array();

        if ($this->is_loaded) {
            return $this->index_content;
        }

        foreach ($this->getRepo()->findAll() as $content) {
            $index_content[$content->getLabel()] = $content->getValue();
        }

        $this->index_content = $index_content;
        $this->is_loaded;

        return $index_content;
    }

    protected function getRepo() {
        if ($this->repo === null) {
            $this->repo = $this->em->getRepository(get_class(new IndexContentEntity()));
        }

        return $this->repo;
    }

}
