<?php

namespace Ls\OrderBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class OrderAdminType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('name', null, array(
                'label' => 'Nazwa',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Wypełnij pole'))
                )
            )
        );
        $builder->add('clientemail', null, array(
                'label' => 'Adres e-mail:',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array(
                        'message' => 'Wypełnij pole'
                    )),
                    new Email(array(
                        'message' => 'Podaj poprawny adres e-mail'
                    ))
                )
            )
        );
        $builder->add('content', TextareaType::class, array(
                'label' => 'Dodatkowe informacje:',
                'required' => false,
            )
        );
        
        $builder->add('kind', EntityType::class, array(
                'label' => 'Rodzaj zamówienia:',
                'class' => 'LsOrderBundle:OrderKind',
                'choice_label' => 'name',
                'required' => false,
            )
        );
        
        $builder->add('status', ChoiceType::class, array(
                'label' => 'Status zamówienia:',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array(
                        'message' => 'Wypełnij pole'
                    ))
                ),
                'choices' => array(
                    'Potwierdzone' => 'Potwierdzone',
                    'W realizacji' => 'W realizacji',
                    'Zakończone' => 'Zakończone'
                ),
                
            )
        );
        $builder->add('submit', SubmitType::class, array(
                'label' => 'Zapisz'
            )
        );
        $builder->add('submit_and_list', SubmitType::class, array(
            'label' => 'Zapisz i przejdź do listy'
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array());
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_order';
    }
}
