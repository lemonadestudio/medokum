$(function() {
    init();
});

/*
 * Triggering functions. 
 */
function init() {
    triggerTestimonialsSlider();
    scrollToTarget();
    showOnScroll();
    showFaqContent();
    formSteps();
    showOneByOne();
    toggleNav();
    closeNav();
    toggleSendOptions();
    triggerFileAdd();
    calculatePrice();
    toggleActiveInput();
    orderFormValidation();
}

/*
 * Triggers testimonials slider animation, with given options.
 */
function triggerTestimonialsSlider() {
    $('.testimonial-slider').bxSlider({
        mode: 'horizontal',
        pager: false,
        nextText: '<img src="/bundles/lscore/front/images/slider-button.png" alt="Następny" />',
        prevText: '<img src="/bundles/lscore/front/images/slider-button.png" alt="Poprzedni" />',
        nextSelector: '#next',
        prevSelector: '#prev'
    });
}

/*
 * Scrolls website to element with id given by button href attribute.
 */
function scrollToTarget() {
    $('.jumper, .navbar ul .last a').on('click', function(e) {
        e.preventDefault();
        
        $('body, html').animate({
            scrollTop: $( $(this).attr('href') ).offset().top 
        }, 700);
    });
}

/*
 * Shows element when they are visible.
 */
function showOnScroll() {
    $(window).scroll(function() {
        var distance = $(document).scrollTop();
        var hiddenElements = $('.hidden');
        
        $(hiddenElements).each(function() {
            var elemDistance = $(this).offset().top;
            if (distance > elemDistance - 620) {
                $(this).removeClass('hidden');
            }
        });
    });
}

function formSteps() {
    var nextBtn = $('#next-step');
    var prevBtn = $('#previous-step');
    
    $(nextBtn).on('click', function(e) {
        e.preventDefault(); 
        $('.order-error').hide();
        
        var allowNextStep = true;
        
        $("#form_order input").each(function() {
            if ($(this).is(":visible")) {
                if ($(this).attr('required')) {
                    if ($(this).val().length > 0) {
                        if ($(this).attr('id') === "form_order_client_email") {
                            if (checkEmail($(this).val()) === false) {
                                allowNextStep = false;
                                $('#required-message').show();
                                $('#required-message').delay(5000).fadeOut();
                            }
                        }
                    } else {
                        allowNextStep = false;
                        $(this).addClass('invalid');
                        $(this).removeClass('valid');
                        $('#required-message').show();
                        $('#required-message').delay(5000).fadeOut();
                    }
                }
            }
        });
        
        if (allowNextStep) {
            $('#required-message').hide();
            toggleNextStep();
            makeNextCircleActive();
            toggleButtonVisible();
        }
    });
    
    $(prevBtn).on('click', function(e) {
        e.preventDefault(); 
        toggleBackStep();
        makePrevCircleActive();
        toggleButtonVisible();
    });
}

// toggling between steps of order. 
function toggleNextStep() {
    var active = $('.active-section');
    
    $(active).next().addClass('active-section');
    $(active).removeClass('active-section');
}

function toggleBackStep() {
    var active = $('.active-section');
    
    $(active).prev().addClass('active-section');
    $(active).removeClass('active-section');
}

/**
 * Shows content of click FAQ.
 */
function showFaqContent() {
    var faqItems = $('.faq-content li');
    
    $(faqItems).each(function() {
        var trigger = $(this).find('.open-faq');
        var currentItem = this;
        
        $(trigger).on('click', function() {
            $(currentItem).toggleClass('automated-height');
            $(currentItem).find('.content').toggle();
            $(currentItem).find('.faq-trigger').toggleClass('fa-plus fa-minus');
        });
    });
}

/**
 * Changes color of next circle in row.
 */
function makeNextCircleActive() {
    var offset;
    var deviceWidth = $(window).width();
    
    if (deviceWidth > 1200) {
        offset = 220;
    } else {
        offset = 145;
    }
    $('.active .smaller-circle-overlay').css('opacity', '1');
    $('.steps .active').addClass('post-active');
    $('.steps .active').next().addClass('active');
    $('.line-overlay').width($('.line-overlay').width() + offset);
    
    //mobile steps
    $('.mobile-steps .step.active').addClass('post-active');
    $('.mobile-steps .step.active').next().addClass('active');
}

/**
 * ^
 * |
 * The same as makeNextCircleActive, but related to previous circle.
 */
function makePrevCircleActive() {
    var offset;
    var deviceWidth = $(window).width();
    
    if (deviceWidth > 1200) {
        offset = 220;
    } else {
        offset = 145;
    }
    $('.post-active .smaller-circle-overlay').last().css('opacity', '0');
    $('.steps .active').last().removeClass('active');
    $('.steps .post-active').last().removeClass('post-active');
    $('.line-overlay').width($('.line-overlay').width() - offset);
    
    $('.mobile-steps .step.active').last().removeClass('active');
    $('.mobile-steps .step.post-active').last().removeClass('post-active');
}

/**
 * Animating children of element one by one, with given delay.
 * @param object element
 */
function showOneByOne() {
    var delay = 0;
    var element = $('#about-us-list li');
    var distance;
    var elementDistance = 0;
    var delayValue = 200;
    
    if (element.length > 1) {
        $(window).scroll(function() {
            distance = $(document).scrollTop();
            elementDistance = $(element).offset().top;
            if (distance > elementDistance - 640) {
                $(element).each(function(){ 
                    $(this).delay(delay).animate({
                        opacity: 1
                    },100);

                delay += delayValue;
                });
            }
        });
    }
}

/**
 * Opens resposive navigation, when trigger is clicked/touched.
 */
function toggleNav() {
    var trigger = $('#nav-trigger');
    var nav = $('.collapse');
    
    $(trigger).on('click', function(e) {
        e.preventDefault();
        
        $(nav).slideDown().css('display', 'flex');
    });
}

/**
 * Closing responsive nav.
 */
function closeNav() {
    var trigger = $('#close-nav');
    var nav = $('.collapse');
    
    $(trigger).on('click', function(e) {
        e.preventDefault();
        
        $(nav).slideUp();
    });
}

/**
 * Toggling options of sending according to chosen option.  
 */
function toggleSendOptions() {
    var leftInputs = [];
    var rightInputs = [];
    
    var leftInputSelector = $('.delivery-way-section .left-part input');
    var rightInputSelector = $('.delivery-way-section .right-part .form-group');
    
    $(leftInputSelector).each(function(index) {
        leftInputs[index] = '.' + $(this).attr('value'); 
    });
    $(rightInputSelector).each(function(index) {
        rightInputs[index] = $(rightInputSelector); 
    });
    
    
    $(leftInputSelector).on('click', function() {
        var className = $(this).val();
        var element = '.' + className;

        $(element).show();
        $('.delivery-way-section input').attr('required', false);
        $(element + ' input').attr('required', 'required');
        $(rightInputSelector).not('.' + className).hide();
    });
}

function toggleButtonVisible() {
    var lastStep = $('#last-step');
    var firstStep = $('#first-step');
    
    var submit = $('#submit');
    var goBack = $('#previous-step');
    
    var paypal = $('#paypal-button');
    
    if (lastStep.hasClass('active-section')) {
        $(submit).show();
        $(paypal).show();
        $('#next-step').hide();
    } else {
        $(submit).hide();
        $(paypal).hide();
        $('#next-step').show();
    }
    
    if (firstStep.hasClass('active-section')) {
        $(goBack).hide();
    } else {
        $(goBack).show().css('display', 'inline-block');
    }
}

/**
 * Triggers uploading of a file, when file button is clicked.
 */
function triggerFileAdd() {
    var $collectionHolder;

    // setup an "add a tag" link
    var $addFileLink = $('.file-button');
    var $newLinkLi = $('<li></li>').append($addFileLink);

    // Get the ul that holds the collection of tags
    $collectionHolder = $('ul.files');

    // add the "add a tag" anchor and li to the tags ul
    $collectionHolder.append($newLinkLi);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find(':input').length);

    $addFileLink.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addFileForm($collectionHolder, $newLinkLi);
        $('.file-input-container ul li input').last().click();
        $('.file-input-container ul li input').on('change', function() {
            addFilename($(this).val());
        });
    });
}

/**
 * Adding new file inputs.
 * @param object $collectionHolder
 * @param object $newLinkLi
 */
function addFileForm($collectionHolder, $newLinkLi) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');

    // get the new index
    var index = $collectionHolder.data('index');

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    var newForm = prototype.replace(/__name__/g, index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add a tag" link li
    var $newFormLi = $('<li></li>').append(newForm);
    $newLinkLi.before($newFormLi);
}

/**
 * Appends new filename to it's list.
 * @param string filename
 */
function addFilename(filename) {
    var wrapper = $('.filenames');
    
    wrapper.append('<li><i class="fa fa-file-text" aria-hidden="true"></i>' + filename.split('\\').pop() + '</li>');
}

/**
 * FUnction calculates price on selecting differnt options.
 */
var discountValue = 0;
var priceList = {};
function calculatePrice() {
    priceList.pagesCount = parseFloat($('[name="form_order[pages]"]').val());
    priceList.firstPagePrice = parseFloat($('[name="form_order[kind]"]:checked').data('firstprice'));
    priceList.nextPagePrice = parseFloat($('[name="form_order[kind]"]:checked').data('nextprice'));
    priceList.percentage = parseFloat($('[name="form_order[priority]"]:checked').data('percentage'));
    var startDay = parseInt($('[name="form_order[priority]"]:checked').data('startday'));
    var endDay = parseInt($('[name="form_order[priority]"]:checked').data('endday'));
    priceList.deliveryPrice = parseFloat($('[name="form_order[format]"]:checked').data('price'));
    
    
    getSumPrice();
    orderDate(startDay, endDay);
    
    $('[name="form_order[pages]"]').on('keyup change load', function(){
        priceList.pagesCount = parseFloat($(this).val());
        getSumPrice();
    });
    
    $('[name="form_order[kind]"]').on('keyup change load', function(){
        priceList.firstPagePrice = parseFloat($(this).data('firstprice'));
        priceList.nextPagePrice = parseFloat($(this).data('nextprice'));
        getSumPrice();
    });
    
    $('[name="form_order[format]"]').on('keyup change load', function(){
        var deliveryPrice = parseFloat($(this).data('price'));
        priceList.deliveryPrice = deliveryPrice;
        getSumPrice();
    });
    
    $('[name="form_order[priority]"]').on('keyup change load', function(){
        priceList.percentage = parseFloat($(this).data('percentage'));
        var startDay = parseInt($(this).data('startday'));
        var endDay = parseInt($(this).data('endday'));
        
        getSumPrice();
        
        $('.datepicker').data('DateTimePicker').destroy();
        $('.datepicker').val("");
        orderDate(startDay, endDay);
    });
    
    $('#form_order').on('keyup change load', 'input, select', function(){
        getSumPrice();
    });
}  

/*
 * @returns {Number}
 */
function getSumPrice() {
    var finalPriceElement = $('#final-price');
    var secondaryPrice = $('#secondary-price');
    var sum = 0;
    var sumWithoutDiscount = 0;
    
    var pagesPrice = priceList.firstPagePrice + ((priceList.pagesCount-1) * priceList.nextPagePrice);
    sum = pagesPrice + (pagesPrice * (priceList.percentage/100)) + priceList.deliveryPrice;
    sum = parseFloat(sum).toFixed(2);
    
    sumWithoutDiscount = sum;
    summaryPrice = sum - (sum * (discountValue/100));
    summaryPrice = parseFloat(summaryPrice).toFixed(3);
    
    //fix. For example in Chrome toFixed(2) for value 0.155 return 0.15 but must return 0.16 
    lastone = summaryPrice.toString().split('').pop();
    if (parseInt(lastone) === 5) {
        summaryPrice = parseFloat(parseFloat(summaryPrice) + parseFloat(0.001)).toFixed(2);
    } else {
        summaryPrice = parseFloat(summaryPrice).toFixed(2);
    }
    //
    
    if (summaryPrice >= 0) {
        $(finalPriceElement).attr('data-price', summaryPrice);
        $(finalPriceElement).html('Cena: <span> &pound; ' + summaryPrice.toString().replace(/\./g, ',') + '</span>');
        $(secondaryPrice).attr('data-price', summaryPrice);
        $(secondaryPrice).html('Cena: <span> &pound; ' + summaryPrice.toString().replace(/\./g, ',') + '</span>');
        
        $('[name="form_order[priority]"]').each(function() {
            var percentageValue = parseFloat($(this).data('percentage'));
            var priorityPrice = (pagesPrice * (percentageValue/100));
            priorityPrice = priorityPrice - (priorityPrice * (discountValue/100));
            $(this).parent().parent().find('.price').html("&pound; +"+ priorityPrice.toFixed(2).toString().replace(/\./g, ','));
        });
        
        $('#form_order_price').val(parseFloat(sumWithoutDiscount).toFixed(2));
        $('#form_order_endPrice').val(parseFloat(summaryPrice).toFixed(2));
        $('#form_order_discount').val(parseFloat(discountValue).toFixed(2));
    } else {
        //location.reload();
    }
    
    return summaryPrice;
}

function setDiscount(value) {
    discountValue = value;
    var finalPriceElement = $('#final-price');
    var secondaryPrice = $('#secondary-price');

    $('.discount').html('<div class="active">Kod rabatowy <span>-'+value+'%</span></div>');
    
    var summaryPrice = getSumPrice();

    if (summaryPrice > 0) {
        $(finalPriceElement).attr('data-price', summaryPrice);
        $(finalPriceElement).html('Cena: <span> &pound; ' + summaryPrice.toString().replace(/\./g, ',') + '</span>');
        $(secondaryPrice).attr('data-price', summaryPrice);
        $(secondaryPrice).html('Cena: <span> &pound; ' + summaryPrice.toString().replace(/\./g, ',') + '</span>');
    } else {
        location.reload();
    }
}

/**
 * Calculating price, depending on number of pages.
 * @param float finalPrice
 * @returns float
 */
function multiplyByPages(finalPrice) {
    var pagesInput = $('#pages-num input');

    $(pagesInput).on('change', function() {
        var value = $(this).val();

        return finalPrice * value;
    });
}

function toggleActiveInput() {
    var inputs = $('.order-kind-section input[type="radio"]');
    var inputsAlternative = $('.order-priority-section input[type="radio"]');
    
    $.each(inputs, function() {
        $(this).on('click', function() {
            var parent = $(this).parent();
            $('.order-kind-section .active-input').removeClass('active-input');
            $('.order-kind-section .whity').removeClass('whity');
            $(this).next().addClass('active-input');
            $(this).addClass('active-input');
        });
        
        if ($(this).is(":checked")) {
            $('.order-kind-section .active-input').removeClass('active-input');
            $('.order-kind-section .whity').removeClass('whity');
            $(this).next().addClass('active-input');
            $(this).addClass('active-input');
        }
    });
    
    $.each(inputsAlternative, function() {
        $(this).on('click', function() {
            var parent = $(this).parent();
            $('.order-priority-section .active-input').removeClass('active-input');
            $('.order-priority-section .whity').removeClass('whity');
            $(this).addClass('active-input');
            $(this).next().addClass('whity');
            $(parent).next().addClass('whity');
        });
        
        if ($(this).is(":checked")) {
            var parent = $(this).parent();
            $('.order-priority-section .active-input').removeClass('active-input');
            $('.order-priority-section .whity').removeClass('whity');
            $(this).addClass('active-input');
            $(this).next().addClass('whity');
            $(parent).next().addClass('whity');
        }
    });
    
}

function orderFormValidation() {
    
    // check inputs
    $('#form_order').on('keyup change focus', 'input, select', function(){
        if ($(this).attr('required') && $(this).val().length > 0) {
            if ($(this).attr('id') === "form_order_client_email") {
                if (checkEmail($(this).val())) {
                    $(this).addClass('valid');
                    $(this).removeClass('invalid');
                } else {
                    $(this).addClass('invalid');
                    $(this).removeClass('valid');
                }
            } else {
                $(this).addClass('valid');
                $(this).removeClass('invalid');
            }
        } else if ($(this).attr('required') && $(this).val().length <= 0) {
            $(this).addClass('invalid');
            $(this).removeClass('valid');
        }
    });
}

// Sprawdza poprawnosc adresu email
function checkEmail(emailAddress) {
    var re = /^[a-zA-Z0-9._\-]+@[a-zA-Z0-9.\-]+\.[a-zA-Z]{2,6}$/;
    if (re.test(emailAddress)) {
        return true;
    }

    return false;
}

function orderDate(startDay, endDay) {
    var today = moment().millisecond(0).second(0).minute(0).hour(0);
    
    var startDateTime = moment(today, "DD-MM-YYYY").add(startDay, 'days');
    var endDateTime = moment(today, "DD-MM-YYYY").add(endDay, 'days');;
    
    $('.datepicker').datetimepicker({
        locale: 'pl',
        minDate: startDateTime,
        maxDate: endDateTime,
        useCurrent: true,
        daysOfWeekDisabled: [0,6],
        disabledDates: disabledDates
    });    
}

//validate count of pages in order
$('#form_order_pages').on('keyup change load', function(){
    var currentValue = $(this).val();
    if (pagesLimit > 0) {
        if (currentValue <= pagesLimit) {
            $(this).val(currentValue);
        } else {
            $(this).val(pagesLimit);
        }
        if (currentValue <= 0) {
            $(this).val(1);
        }
    } else {
        if (currentValue <= 10) {
            $(this).val(currentValue);
        } else {
            $(this).val(10);
        }
        if (currentValue <= 0) {
            $(this).val(1);
        }
    }
});

//validate order form before submit
$('#form_order #submit').click(function (e) {
    e.preventDefault();
    
    var allowSubmit = true;
    $("#form_order input").each(function() {
        if ($(this).is(":visible")) {
            if ($(this).attr('required')) {
                if ($(this).val().length > 0) {
                    if ($(this).attr('id') === "form_order_client_email") {
                        if (checkEmail($(this).val()) === false) {
                            allowSubmit = false;
                            $('#required-message').show();
                            $('#required-message').delay(5000).fadeOut();
                        }
                    }
                } else {
                    allowSubmit = false;
                    $(this).addClass('invalid');
                    $(this).removeClass('valid');
                    $('#required-message').show();
                    $('#required-message').delay(5000).fadeOut();
                }
            }
        }
    });
    
    if (!allowSubmit) {
        return false;
    } else {
        $("#form_order").submit();
    }
});