<?php

namespace Ls\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * FileType
 * @ORM\Table(name="files_types")
 * @ORM\Entity
 */
class FileType {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $name;
    
    /**
     *  @ORM\OneToMany(
     *   targetEntity="File",
     *   mappedBy="type",
     *   cascade={"all"}
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $files;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * Constructor
     */
    public function __construct() {
        $this->created_at = new \DateTime();
        $this->files = new ArrayCollection();
    }

    public function __toString()
    {
        if (is_null($this->getName())) {
            return 'NULL';
        }

        return $this->getName();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return OrderKind
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return OrderKind
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Add file
     *
     * @param \Ls\OrderBundle\Entity\File $file
     *
     * @return FileType
     */
    public function addFile(\Ls\OrderBundle\Entity\File $file)
    {
        $this->file[] = $file;
    
        return $this;
    }

    /**
     * Remove file
     *
     * @param \Ls\OrderBundle\Entity\File $file
     */
    public function removeFile(\Ls\OrderBundle\Entity\File $file)
    {
        $this->file->removeElement($file);
    }

    /**
     * Get file
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }
}
