-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Czas generowania: 18 Sty 2019, 16:04
-- Wersja serwera: 10.2.19-MariaDB-log-cll-lve
-- Wersja PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `mastermi_medokum`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `disabled_dates`
--

CREATE TABLE `disabled_dates` (
  `id` int(11) NOT NULL,
  `disabled_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `discount`
--

CREATE TABLE `discount` (
  `id` int(11) NOT NULL,
  `available_uses` int(11) DEFAULT NULL,
  `uses_count` int(11) DEFAULT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `discount` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `discount`
--

INSERT INTO `discount` (`id`, `available_uses`, `uses_count`, `value`, `created_at`, `end_date`, `discount`) VALUES
(1, 10, 10, 'ut6z3khV', '2017-07-18 11:55:25', '2017-07-30 18:58:00', 10),
(2, 1, 0, 'RRwA7FIP', '2017-07-18 11:55:32', '2017-07-17 00:00:00', 20),
(3, 1, 1, 'ktaWLKDK', '2017-07-18 11:55:42', '2017-07-31 00:00:00', 99);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `faq`
--

CREATE TABLE `faq` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `faq`
--

INSERT INTO `faq` (`id`, `title`, `slug`, `content_short_generate`, `content_short`, `content`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`, `category_id`) VALUES
(6, 'W jaki sposób przesłać dokument do tłumaczenia?', 'w-jaki-sposob-przeslac-dokument-do-tlumaczenia', 0, 'Dokument do tłumaczenia można przesłać za pomoca funkcji \"Dodaj plik z tekstem\" w kroku 1 formularza \"Złóż zamówienie\". Pliki można przesyłać w formacie: .pdf, .jpg, .jpeg, .tiff, .xdoc, .doc.', '<p>\r\n	Dokument do tłumaczenia można przesłać za pomoca funkcji &quot;Dodaj plik z tekstem&quot; w kroku 1 formularza &quot;Zł&oacute;ż zam&oacute;wienie&quot;. Pliki można przesyłać w formacie: .pdf, .jpg, .jpeg, .tiff, .xdoc, .doc.</p>', 0, 'W jaki sposób przesłać dokument do tłumaczenia?', 'jaki, sposób, przesłać, dokument, tłumaczenia?, Dokument, tłumaczenia, można, pomoca, funkcji', 'Dokument do tłumaczenia można przesłać za pomoca funkcji \"Dodaj plik z tekstem\" w kroku 1 formularza \"Złóż zamówienie\". Pliki można przesyłać w formacie:...', '2017-08-31 13:25:34', NULL, 1),
(7, 'W jaki sposób otrzymam gotowe tłumaczenie?', 'w-jaki-sposob-otrzymam-gotowe-tlumaczenie', 0, 'Oferujemy dwie opcje odbioru tłumaczenia. Gotowe tłumaczenie może być przesłane jako dokument PDF bezpośrednio na podany przez klienta adres e-mail. Za dodatkową opłatą istnieje również możliwość otrzymania tłumaczenia listownie na wskazany przez...', '<p>\r\n	Oferujemy dwie opcje odbioru tłumaczenia. Gotowe tłumaczenie może być przesłane jako dokument PDF bezpośrednio na podany przez klienta adres e-mail. Za dodatkową opłatą istnieje r&oacute;wnież możliwość otrzymania tłumaczenia listownie na wskazany przez klienta adres pocztowy na terenie Wielkiej Brytanii. W takim wypadku kopia tłumaczenia zostanie dodatkowo przesłana na podany przez klienta adres e-mail.</p>', 0, 'W jaki sposób otrzymam gotowe tłumaczenie?', 'jaki, sposób, otrzymam, gotowe, tłumaczenie?, Oferujemy, dwie, opcje, odbioru, tłumaczenia', 'Oferujemy dwie opcje odbioru tłumaczenia. Gotowe tłumaczenie może być przesłane jako dokument PDF bezpośrednio na podany przez klienta adres e-mail. Za...', '2017-08-31 13:26:07', NULL, 1),
(8, 'Jakie dokumenty mogę przetłumaczyć?', 'jakie-dokumenty-moge-przetlumaczyc', 0, 'Za pośrednictwem aplikacji Medokum można zlecić tłumaczenie wyłącznie dokumentacji medycznej. Typy dokumentów wyszczególnione są na liście w kroku 1 zamówienia.', '<p>\r\n	Za pośrednictwem aplikacji Medokum można zlecić tłumaczenie wyłącznie dokumentacji medycznej. Typy dokument&oacute;w wyszczeg&oacute;lnione są na liście w kroku 1 zam&oacute;wienia.</p>', 0, 'Jakie dokumenty mogę przetłumaczyć?', 'Jakie, dokumenty, mogę, przetłumaczyć?, pośrednictwem, aplikacji, Medokum, można, zlecić, tłumaczenie', 'Za pośrednictwem aplikacji Medokum można zlecić tłumaczenie wyłącznie dokumentacji medycznej. Typy dokumentów wyszczególnione są na liście w kroku 1...', '2017-08-31 13:26:48', NULL, 1),
(9, 'Jak mogę zgłosić uwagi do tłumaczenia?', 'jak-moge-zglosic-uwagi-do-tlumaczenia', 0, 'Swoje uwagi dotyczące tłumaczenia, zarówne te pozytywne, jak i negatywne, można przekazać, pisząc pod adres: hello@medokum.co.uk. Postaram się je rozpatrzeć w możliwie krótkim czasie i w stosownych przypadkach skontaktować się z klientem w celu ich omówie', '<p>\r\n	Swoje uwagi dotyczące tłumaczenia, zar&oacute;wne te pozytywne, jak i negatywne, można przekazać, pisząc pod adres: hello@medokum.co.uk. Postaram się je rozpatrzeć w możliwie kr&oacute;tkim czasie i w stosownych przypadkach skontaktować się z klientem w celu ich om&oacute;wienia.</p>', 0, 'Jak mogę złosić uwagi do tłumaczenia?', 'mogę, złosić, uwagi, tłumaczenia?, Swoje, dotyczące, tłumaczenia, zarówne, pozytywne, negatywne', 'Swoje uwagi dotyczące tłumaczenia, zarówne te pozytywne, jak i negatywne, można przekazać, pisząc pod adres: hello@medokum.co.uk. Postaram się je...', '2017-08-31 13:27:13', '2017-08-31 13:34:44', 2),
(10, 'Jak mogę zapłacić za złożone zamówienie?', 'jak-moge-zaplacic-za-zlozone-zamowienie', 0, 'Za złożone zamówienie można zapłacić kartą debetową, kartą kredytową lub za pośrednictwem serwisu PayPal. Płatności dokonuje się w kroku 5 formularza \"Złóż zamówienie\".', '<p>\r\n	Za złożone zam&oacute;wienie można zapłacić kartą debetową, kartą kredytową lub za pośrednictwem serwisu PayPal. Płatności dokonuje się w kroku 5 formularza &quot;Zł&oacute;ż zam&oacute;wienie&quot;.</p>', 0, 'Jak mogę zapłacić za złożone zamówienie?', 'mogę, zapłacić, złożone, zamówienie?, zamówienie, można, kartą, debetową, kredytową, pośrednictwem', 'Za złożone zamówienie można zapłacić kartą debetową, kartą kredytową lub za pośrednictwem serwisu PayPal. Płatności dokonuje się w kroku 5 formularza...', '2017-08-31 13:27:46', NULL, 1),
(11, 'Czy w cenę usługi wliczony jest VAT?', 'czy-w-cene-uslugi-wliczony-jest-vat', 0, 'Tak, w cenę usługi wliczony jest podatek VAT w wysokości 20%. Jeśli klient chciałby otrzymać fakturę VAT za wykonaną usługę, prosimy o kontakt pod adresem: hello@medokum.co.uk.', '<p>\r\n	Tak, w cenę usługi wliczony jest podatek VAT w wysokości 20%. Jeśli klient chciałby otrzymać fakturę VAT za wykonaną usługę, prosimy o kontakt pod adresem: hello@medokum.co.uk.</p>', 0, 'Czy w cenę usługi wliczony jest VAT?', 'cenę, usługi, wliczony, jest, VAT?, podatek, wysokości, Jeśli, klient, chciałby', 'Tak, w cenę usługi wliczony jest podatek VAT w wysokości 20%. Jeśli klient chciałby otrzymać fakturę VAT za wykonaną usługę, prosimy o kontakt pod...', '2017-08-31 13:28:10', '2017-08-31 13:28:41', 2),
(12, 'Czy muszę się rejestrować?', 'czy-musze-sie-rejestrowac', 0, 'Przy składaniu zamówienia nie ma potrzeby rejestrowania się. Wystarczy podać swój adres e-mail.', '<p>\r\n	Przy składaniu zam&oacute;wienia nie ma potrzeby rejestrowania się. Wystarczy podać sw&oacute;j adres e-mail.</p>', 0, 'Czy muszę się rejestrować?', 'muszę, się, rejestrować?, Przy, składaniu, zamówienia, potrzeby, rejestrowania, Wystarczy, podać', 'Przy składaniu zamówienia nie ma potrzeby rejestrowania się. Wystarczy podać swój adres e-mail.', '2017-08-31 13:29:07', NULL, 1),
(13, 'Czy mogę zamówić tłumaczenie z języka angielskiego na język polski?', 'czy-moge-zamowic-tlumaczenie-z-jezyka-angielskiego-na-jezyk-polski', 0, 'W chwili obecnej nie oferujemy opcji zamawiania tłumaczeń dokumentacji medycznej z języka angielskiego na język polski bezpośrednio przez aplikację Medokum. W tej sprawie prosimy wysłać zapytanie pod adres: hello@medokum.co.uk.', '<p>\r\n	W chwili obecnej nie oferujemy opcji zamawiania tłumaczeń dokumentacji medycznej z języka angielskiego na język polski bezpośrednio przez aplikację Medokum. W tej sprawie prosimy wysłać zapytanie pod adres: hello@medokum.co.uk.</p>', 0, 'Czy mogę zamówić tłumaczenie z języka angielskiego na język polski?', 'mogę, zamówić, tłumaczenie, języka, angielskiego, język, polski?, chwili, obecnej, oferujemy', 'W chwili obecnej nie oferujemy opcji zamawiania tłumaczeń dokumentacji medycznej z języka angielskiego na język polski bezpośrednio przez aplikację...', '2017-08-31 13:29:41', NULL, 2),
(14, 'Czy mogę zamówić tłumaczenie przysięgłe?', 'czy-moge-zamowic-tlumaczenie-przysiegle', 0, 'W chwili obecnej nie oferujemy tłumaczeń uwierzytelnionych, potocznie zwanych przysięgłymi, ponieważ tego rodzaju tłumaczenia nie są wymagane przez instytucje brytyjskie. Jedynie instytucje polskie wymagają, aby w stosownych przypadkach...', '<p>\r\n	W chwili obecnej nie oferujemy tłumaczeń uwierzytelnionych, potocznie zwanych przysięgłymi, ponieważ tego rodzaju tłumaczenia nie są wymagane przez instytucje brytyjskie. Jedynie instytucje polskie wymagają, aby w stosownych przypadkach tłumaczenie opatrzone było pieczątką tłumacza przysięgłego.</p>', 0, 'Czy mogę zamówić tłumaczenie przysięgłe?', 'mogę, zamówić, tłumaczenie, przysięgłe?, chwili, obecnej, oferujemy, tłumaczeń, uwierzytelnionych, potocznie', 'W chwili obecnej nie oferujemy tłumaczeń uwierzytelnionych, potocznie zwanych przysięgłymi, ponieważ tego rodzaju tłumaczenia nie są wymagane przez...', '2017-08-31 13:30:04', NULL, 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `faq_categories`
--

CREATE TABLE `faq_categories` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `faq_categories`
--

INSERT INTO `faq_categories` (`id`, `title`, `slug`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`) VALUES
(1, 'Proste pytania', '', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL),
(2, 'Trudne pytania', '', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `file`
--

CREATE TABLE `file` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `file`
--

INSERT INTO `file` (`id`, `name`, `created_at`, `url`, `order_id`, `type_id`, `file`) VALUES
(73, NULL, '2017-07-20 15:18:02', NULL, NULL, NULL, '/tmp/phpOxhpQN'),
(75, NULL, '2017-07-20 15:19:56', NULL, NULL, NULL, '/tmp/phpw8eGoR'),
(77, NULL, '2017-07-21 12:31:21', NULL, NULL, NULL, '/tmp/phpWzGEUW');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `files_types`
--

CREATE TABLE `files_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `files_types`
--

INSERT INTO `files_types` (`id`, `name`, `created_at`) VALUES
(1, 'Historia zdrowia i choroby', '2017-05-15 12:27:32'),
(4, 'Wypis ze szpitala', '2017-08-28 13:00:08'),
(5, 'Zwolnienie lekarskie', '2017-08-31 13:39:14'),
(6, 'Historia choroby', '2017-08-31 13:39:26'),
(7, 'Karta noworodka', '2017-08-31 13:39:32'),
(8, 'Karta indywidualnej opieki pielęgniarskiej', '2017-08-31 13:39:39'),
(9, 'Karta indywidualnej opieki prowadzonej przez położną', '2017-08-31 13:39:45'),
(10, 'Karta wizyty patronażowej', '2017-08-31 13:39:51'),
(11, 'Karta wywiadu środowiskowo-rodzinnego', '2017-08-31 13:39:58'),
(12, 'Karta uodpornienia', '2017-08-31 13:40:02'),
(13, 'Skierowanie do szpitala', '2017-08-31 13:40:08'),
(14, 'Skierowanie na badanie diagnostyczne, konsultację lub leczenie', '2017-08-31 13:40:13'),
(15, 'Karta przebiegu ciąży', '2017-08-31 13:40:17'),
(16, 'Książeczka zdrowia dziecka', '2017-08-31 13:40:22'),
(17, 'Karta informacyjna z leczenia szpitalnego', '2017-08-31 13:40:29'),
(18, 'Pisemna informacja lekarza leczącego pacjenta w poradni specjalistycznej', '2017-08-31 13:40:34'),
(19, 'Książeczka szczepień', '2017-08-31 13:40:38'),
(20, 'Zaświadczenie lekarskie', '2017-08-31 13:40:44'),
(21, 'Orzeczenie lekarskie', '2017-08-31 13:40:49'),
(22, 'Opinia lekarska', '2017-08-31 13:40:54'),
(23, 'Wynik badania laboratoryjnego', '2017-08-31 13:40:58'),
(24, 'Wynik badania histopatologicznego', '2017-08-31 13:41:04'),
(25, 'Wynik badania cytologicznego', '2017-08-31 13:41:09'),
(26, 'Wynik badania obrazowego (np. RTG, USG, TK, MRI, PET, SPECT, scyntygrafii, densytometrii)', '2017-08-31 13:41:16'),
(27, 'Wynik konsultacji specjalistycznej', '2017-08-31 13:41:23'),
(28, 'Protokół zabiegu operacyjnego', '2017-08-31 13:41:30'),
(29, 'Inny typ dokumentacji medycznej', '2017-08-31 13:41:39');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `on_list` tinyint(1) NOT NULL,
  `attachable` tinyint(1) NOT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gallery_photo`
--

CREATE TABLE `gallery_photo` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `index_content`
--

CREATE TABLE `index_content` (
  `id` int(11) NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `index_content`
--

INSERT INTO `index_content` (`id`, `label`, `description`, `value`) VALUES
(1, 'top_title', 'Etykieta - Potrzebujesz profesjonalnego tłumaczenia swojej dokumentacji medycznej?', 'Potrzebujesz profesjonalnego tłumaczenia dokumentacji medycznej?'),
(2, 'first_step_title', 'Etykieta - Masz do przetłumaczenia dokument medyczny?', 'Masz do przetłumaczenia\r\n<span>dokument medyczny?</span>'),
(3, 'second_step_title_2', 'Etykieta - Wolisz zlecić tłumaczenie doświadczonemu lekarzowi', 'Wolisz zlecić tłumaczenie\r\n<span>doświadczonemu lekarzowi?</span>'),
(4, 'second_step_title_1', 'Etykieta - Chcesz mieć 100% gwarancję pufnosci?', 'Chcesz mieć\r\n<span>100% gwarancję poufnosci?</span>'),
(5, 'third_step_title', 'Etykieta - Zależy Ci na czasie i cenie?', 'Zależy Ci\r\n<span>na czasie i cenie?</span>'),
(6, 'end_step_title', 'Etykieta - Trafiłeś we właściwe miejsce!', 'Trafiłeś we właściwe miejsce!'),
(7, 'index_list', 'Lista (Każdy punkt oddzielony przecinkiem)', 'Precyzja i wysoka jakość, Oferta dopasowana do potrzeb, Szybka obsługa, Konkurencyjne ceny, Gwarancja poufności, Tłumaczenia wykonywane przez lekarza'),
(8, 'footer_order_title', 'Etykieta - Sprawdź koszt tłumaczenia  i złóż zamowienie w kilku prostych krokach', 'Sprawdź koszt tłumaczenia \r\ni złóż zamowienie w kilku prostych krokach'),
(9, 'footer_info_title', 'Etykieta - Nadal masz wątpliwości?', 'Nadal masz wątpliwości?'),
(10, 'footer_info_content', 'Nadal masz wątpliwości? - Treść', '<p>\r\n	Zajrzyj do dzialu <strong><a href=\"/faq\">FAQ </a></strong>i sprawdź odpowiedzi na najczęściej zadawane pytania lub podziel się z nami swoimi wątpliościami</p>');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `mailtemplates`
--

CREATE TABLE `mailtemplates` (
  `id` int(11) NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `variables` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `mailtemplates`
--

INSERT INTO `mailtemplates` (`id`, `label`, `description`, `variables`, `title`, `value`) VALUES
(1, 'new_order', 'Wiadomość o przyjęciu zamuwienia', '%order_number% - numer zamówienia\r\n%name% - Imię i nazwisko\r\n%type% - Typ dokumentu\r\n%pages% - Ilość stron\r\n%kind% - Rodzaj usługi\r\n%finish_date% - Interesująca data oddania tłumaczenia\r\n%priority% - Priorytet zamówienia\r\n%delivery% - Sposób odbioru tłumaczenia\r\n%email% - Adres e-mail\r\n%address% - Adres\r\n%price% - Cena końcowa', 'Przyjęcie zamówienia', '<p>\r\n	<span style=\"font-size:16px;\">Witaj,&nbsp;%name% </span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style=\"font-size:16px;\">Twoje zam&oacute;wienie №%order_number%&nbsp;zostało przyjęte. </span></p>\r\n<p>\r\n	<span style=\"font-size:16px;\">O zmianach w statusie zostaniesz poinformowany drogą mailową. </span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style=\"font-size:16px;\">Szczeg&oacute;ły zam&oacute;wienia:</span></p>\r\n<p>\r\n	<span style=\"font-family:arial,helvetica,sans-serif;\"><span style=\"font-size:16px;\">Typ dokumentu: %type%</span></span></p>\r\n<p>\r\n	<span style=\"font-family:arial,helvetica,sans-serif;\"><span style=\"font-size:16px;\">Ilość stron: %pages%</span></span></p>\r\n<p>\r\n	<span style=\"font-family:arial,helvetica,sans-serif;\"><span style=\"font-size:16px;\">Priorytet: %priority%</span></span></p>\r\n<p>\r\n	<span style=\"font-family:arial,helvetica,sans-serif;\"><span style=\"font-size:16px;\">Spos&oacute;b odbioru tłumaczenia: %delivery%</span></span></p>\r\n<p>\r\n	<strong><span style=\"font-family:arial,helvetica,sans-serif;\"><span style=\"font-size:16px;\">Cena: %price%</span></span></strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p style=\"font-family: \'Arial\', serif; margin: 0; font-size: 12px;\">\r\n	Pozdrawiamy,</p>\r\n<p style=\"font-family: \'Arial\', serif; margin: 0; font-size: 13px; margin-top: 5px; margin-bottom: 10px;\">\r\n	<strong>Zesp&oacute;ł Medokum</strong></p>\r\n<p>\r\n	<img src=\"http://medokum.co.uk/medokum_stopka_logo.jpg\" /></p>\r\n<p style=\"font-family: \'Arial\', serif; margin: 0; font-size: 11px; margin-top: 16px;\">\r\n	<img src=\"http://medokum.co.uk/stopka_mail.png\" /><span style=\"position:relative;top:-2px;margin-left:6px\">hello@medokum.co.uk</span></p>\r\n<p style=\"font-family: \'Arial\', serif; margin: 0; font-size: 11px; margin-top: 5px;\">\r\n	<img src=\"http://medokum.co.uk/stopka_adress.png\" style=\"margin-left:1px;\" /><span style=\"position:relative;top:-4px;margin-left:9px\">Office 118, 20 Winchcombe Street, Cheltenham GL52 2LY, United Kingdom</span></p>\r\n<p style=\"font-family: \'Arial\', serif; margin: 0; font-size: 11px; margin-top: 16px;\">\r\n	&copy; Medokum All rights reserved. Designed and developed by: Lemonade Studio.</p>');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `menu_item`
--

CREATE TABLE `menu_item` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route_parameters` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `onclick` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blank` tinyint(1) DEFAULT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `menu_item`
--

INSERT INTO `menu_item` (`id`, `parent_id`, `location`, `type`, `title`, `route`, `route_parameters`, `url`, `onclick`, `blank`, `arrangement`) VALUES
(1, NULL, 'menu_top', 'route', 'Co robimy', 'ls_page_show', '{\"slug\":\"co-robimy\"}', '/co-robimy', NULL, 0, 1),
(2, NULL, 'menu_top', 'url', 'Kontakt', NULL, NULL, '#kontakt', NULL, 0, 3),
(3, NULL, 'menu_top', 'route', 'FAQ', 'ls_faq_list', NULL, '/faq', NULL, 0, 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `show_main` tinyint(1) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `published_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `opinions`
--

CREATE TABLE `opinions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `opinions`
--

INSERT INTO `opinions` (`id`, `name`, `city`, `content`, `created_at`) VALUES
(1, 'Bartosz S.', 'Chester', '<p>\r\n	Jakość usługi oraz spos&oacute;b zredagowania dokument&oacute;w na najwyższym poziomie. Natomiast jeśli chodzi o cenę, to osobiście uważam, że warto zapłacić więcej za dobrej jakości towar bądź usługę. Wtedy można mieć gwarancję profesjonalnego podejścia do sprawy, szczeg&oacute;lnie jeśli w grę wchodzi dokumentacja lekarska, gdzie najdrobniejszy szczeg&oacute;ł ma ogromne znaczenie. Jeszcze raz serdecznie dziękuję za usługę.</p>', '2017-08-28 15:50:04'),
(2, 'Magdalena W.', 'Hayes', '<p>\r\n	Bardzo dziękuję za szybkie wykonanie usługi!</p>', '2017-08-28 15:50:21'),
(3, 'Jerzy K.', 'Liverpool', '<p>\r\n	Bardzo jestem zadowolony z jakości tłumaczenia. Z dokumentacją medyczną nie chciałem ryzykować, dlatego wolałem, aby przetłumaczył mi ją lekarz. Tłumaczenie i obsługa były bardzo profesjonalne. Gorąco polecam!</p>', '2017-08-29 12:24:58'),
(4, 'Aneta L.', 'Coventry', '<p>\r\n	Potrzebowałam szybko przetłumaczyć wynik badania USG wykonanego w Polsce. Bardzo zależało mi na czasie i, nie ukrywam, że też na cenie. Cieszę się, że tę sprawę udało mi się tak sprawnie załatwić. Dziękuję!</p>', '2017-08-29 12:35:00'),
(5, 'Patrycja B.', 'Londyn', '<p>\r\n	Szukałam tłumacza, kt&oacute;ry przetłumaczyłby mi dokumenty z leczenia psychiatrycznego syna. Oczywiście ze względu na poufny charakter sprawy nie chciałam się z tym zwracać do znajomych. Fajnie, że mogłam po prostu przesłać dokumenty do tłumaczenia przez portal Medokum bez ujawniania dodatkowych szczeg&oacute;ł&oacute;w czy wyjaśniania sytuacji. Szybko, poufnie i w rozsądnej cenie. Polecam!</p>', '2017-08-29 12:55:46');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `status` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `kind_id` int(11) DEFAULT NULL,
  `types` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `finish_date` datetime DEFAULT NULL,
  `priority_id` int(11) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `format` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `pages` int(11) NOT NULL,
  `price` varchar(254) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_price` varchar(254) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` varchar(254) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_id` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payer_id` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_state` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `orders`
--

INSERT INTO `orders` (`id`, `name`, `client_email`, `content`, `created_at`, `status`, `kind_id`, `types`, `finish_date`, `priority_id`, `address`, `format`, `pages`, `price`, `end_price`, `discount`, `payment_id`, `payer_id`, `payment_state`) VALUES
(34, 'Oleh Vaskiv', 'oleh@lemonadestudio.pl', NULL, '2017-07-20 15:17:12', 'Przyjęte', 3, 'Wypis ze szpitala', '2017-07-20 00:00:00', 3, NULL, 'pdf', 2, '42', '37.80', '10', NULL, NULL, NULL),
(35, 'Oleh Vaslov', 'oleh@lemonadestudio.pl', NULL, '2017-07-20 15:18:01', 'Przyjęte', 3, 'Wypis ze szpitala', '2017-07-20 00:00:00', 3, NULL, 'pdf', 2, '42', '37.80', '10', NULL, NULL, NULL),
(36, 'OV', 'oleh@lemonadestudio.pl', NULL, '2017-07-20 15:19:55', 'Przyjęte', 3, 'Wypis ze szpitala', '2017-07-31 00:00:00', 3, NULL, 'pdf', 2, '42', '42.00', '0', NULL, NULL, NULL),
(37, 'Oleh Vaskiv', 'vaskiv7@gmail.com', NULL, '2017-07-21 12:31:21', 'Przyjęte', 3, 'Wypis ze szpitala', '2017-07-31 00:00:00', 3, NULL, 'pdf', 2, '42', '37.80', '10', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `orders_kind`
--

CREATE TABLE `orders_kind` (
  `id` int(11) NOT NULL,
  `defaultValue` tinyint(1) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_price` decimal(10,2) NOT NULL,
  `next_price` decimal(10,2) NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `orders_kind`
--

INSERT INTO `orders_kind` (`id`, `defaultValue`, `name`, `first_price`, `next_price`, `content`, `created_at`) VALUES
(1, 1, 'Standardowa', '30.99', '15.99', '<ul>\r\n	<li style=\"line-height: 18px;\">\r\n		<div style=\"line-height: 18px;\">\r\n			Tłumaczenie całej treści dokumentu z opisem pieczątek i fragment&oacute;w o treści niemedycznej, np. nagł&oacute;wk&oacute;w adresowych, pouczeń, uwag itp.</div>\r\n	</li>\r\n	<li style=\"line-height: 18px;\">\r\n		<div style=\"line-height: 18px;\">\r\n			Zachowanie rozmieszczenia tekstu orygina</div>\r\n	</li>\r\n</ul>', '2017-07-20 15:12:10'),
(2, 0, 'Kompleksowa', '35.99', '17.99', '<ul>\r\n	<li style=\"line-height: 18px;\">\r\n		Tłumaczenie całej treści dokumentu z opisem pieczątek i fragment&oacute;w o treści niemedycznej, np. nagł&oacute;wk&oacute;w adresowych, pouczeń, uwag itp.</li>\r\n	<li style=\"line-height: 18px;\">\r\n		Zachowanie rozmieszczenia tekstu oryginału</li>\r\n	<li style=\"line-height: 18px;\">\r\n		Dodatkowa w</li>\r\n</ul>', '2017-07-20 15:12:27'),
(3, 0, 'Podstawowa', '25.99', '12.99', '<ul>\r\n	<li style=\"line-height: 18px;\">\r\n		Tłumaczenie najbardziej istotnych fragment&oacute;w dokumentu bez opisu pieczątek i fragment&oacute;w o treści niemedycznej, np. nagł&oacute;wk&oacute;w adresowych, pouczeń, uwag itp.</li>\r\n</ul>', '2017-07-20 15:12:46');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `orders_priority`
--

CREATE TABLE `orders_priority` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `percentage` decimal(10,2) NOT NULL,
  `defaultValue` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `start_day` int(11) DEFAULT NULL,
  `end_day` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `orders_priority`
--

INSERT INTO `orders_priority` (`id`, `name`, `percentage`, `defaultValue`, `created_at`, `start_day`, `end_day`) VALUES
(1, 'Podstawowy', '0.00', 0, '2017-07-20 15:11:23', 7, 30),
(2, 'Standard', '10.00', 1, '2017-07-20 15:11:29', 4, 30),
(3, 'Ekspres', '30.00', 0, '2017-07-20 15:11:36', 2, 30);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `page`
--

INSERT INTO `page` (`id`, `gallery_id`, `title`, `slug`, `old_slug`, `content_short_generate`, `content_short`, `content`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Co robimy', 'co-robimy', 'co-robimy', 0, 'Potrzebujesz profesjonalnego tłumaczenia dokumentacji medycznej w dogodnym dla Państwa terminie i po przystępnej cenie? Złóż zamówienie w kilka kliknięć!', '<h1 class=\"naglowek\" style=\"line-height: 27px;\">\r\n	Chcesz kontynuować leczenie w Wielkiej Brytanii i lekarz poprosił Cię o dostarczenie dokumentacji medycznej z Polski? Pracodawca wymaga przedłożenia zaświadczenia lekarskiego, aby udzielić Ci urlopu? Jest wiele sytuacji, w kt&oacute;rych możesz potrzebować dobrej jakości tłumaczenia dokumentacji medycznej. Chętnie pomożemy!</h1>\r\n<h1 class=\"naglowek\">\r\n	&nbsp;</h1>\r\n<div style=\"line-height: 18px;\">\r\n	<span style=\"font-size: 14px; line-height: 21px;\">Tłumaczenia medyczne są naszą specjalnością: przez&nbsp;<strong style=\"line-height: 18px;\">10 lat istnienia na rynku brytyjskim</strong>&nbsp;wykonaliśmy ich ponad p&oacute;łtora tysiąca i obsłużyliśmy prawie tysiąc klient&oacute;w.&nbsp;To wieloletnie doświadczenie w branży tłumaczeń medycznych i farmaceutycznych dało nam solidne podstawy, by rozszerzyć naszą ofertę o aplikację internetową, przez kt&oacute;rą można w łatwy spos&oacute;b&nbsp;<strong style=\"line-height: 18px;\">zlecić tłumaczenie dokumentacji medycznej</strong>.</span></div>\r\n<div style=\"line-height: 18px;\">\r\n	&nbsp;</div>\r\n<div style=\"line-height: 18px;\">\r\n	<span style=\"font-size: 14px; line-height: 21px;\">Doskonale rozumiemy, że nie zawsze dysponujesz czasem, aby poszukiwać rzetelnego tłumacza, kt&oacute;ry byłby w stanie podjąć się przekładu tekstu medycznego na odpowiadających Ci warunkach wsp&oacute;łpracy.&nbsp;Wielokrotnie zgłaszały się do nas osoby, kt&oacute;re rozczarowane były usługami zaoferowanymi zar&oacute;wno przez tradycyjne biura tłumaczeń, jak i indywidualnych tłumaczy. Zastrzeżenia te dotyczyły najczęściej czterech aspekt&oacute;w:&nbsp;<strong style=\"line-height: 18px;\">jakości, ceny, terminu realizacji&nbsp;</strong>i<strong style=\"line-height: 18px;\">&nbsp;poufności</strong>.</span></div>\r\n<div style=\"line-height: 18px;\">\r\n	&nbsp;</div>\r\n<div style=\"line-height: 18px;\">\r\n	<span style=\"font-size: 14px; line-height: 21px;\">Idea naszej aplikacji&nbsp;<strong style=\"line-height: 18px;\">Medokum</strong>&nbsp;opiera się właśnie na tych czterech filarach. Uważamy, że każdy zasługuje na to, aby jego dokumentacja medyczna była rzetelnie przetłumaczona w dogodnym dla niego terminie i za odpowiadającą mu cenę a jednocześnie przy zachowaniu wszelkich zasad poufności.</span></div>\r\n<div style=\"line-height: 18px;\">\r\n	&nbsp;</div>\r\n<div style=\"line-height: 18px;\">\r\n	<span style=\"font-size: 14px; line-height: 21px;\">Wychodząc naprzeciw oczekiwaniom naszych klient&oacute;w, stworzyliśmy usługę, dzięki kt&oacute;rej zlecenie tłumaczenia dokumentacji medycznej przestaje być problemem. Aby zapewnić dokładność przekładu, w naszym zespole pracują:&nbsp;<strong style=\"line-height: 18px;\">lekarz, inżynier chemii i filolog</strong>. Dzięki takiemu połączeniu wiedzy medycznej z doświadczeniem językowym jesteśmy w stanie przygotować tłumaczenie medyczne idealnie odzwierciedlające oryginał. W odr&oacute;żnieniu od innych usługodawc&oacute;w dajemy r&oacute;wnież&nbsp;<strong style=\"line-height: 18px;\">możliwość wyboru ceny i terminu</strong>&nbsp;realizacji tłumaczenia. Natomiast nasze bezkompromisowe podejście do&nbsp;<strong style=\"line-height: 18px;\">poufności informacji</strong>&nbsp;gwarantuje, że przekazane nam dane nigdy nie zostaną ujawnione osobom postronnym. A złożenie zam&oacute;wienia sprowadza się zaledwie do kilku kliknięć!</span></div>\r\n<div style=\"line-height: 18px;\">\r\n	&nbsp;</div>\r\n<div style=\"line-height: 18px;\">\r\n	<span style=\"font-size: 14px; line-height: 21px;\"><a href=\"/zloz-zamowienie\" style=\"line-height: 18px;\">Kliknij, aby złożyć zam&oacute;wienie</a>.</span></div>\r\n<div style=\"line-height: 18px;\">\r\n	&nbsp;</div>\r\n<div style=\"line-height: 18px;\">\r\n	<span style=\"font-size: 14px; line-height: 21px;\">Pytania? Zapraszamy do działu&nbsp;<a href=\"/faq/\" style=\"line-height: 18px;\">FAQ</a>.</span></div>\r\n<div>\r\n	&nbsp;</div>', 0, 'Co robimy', 'robimy', NULL, '2017-05-23 11:55:54', '2017-08-31 13:23:02');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `setting`
--

INSERT INTO `setting` (`id`, `label`, `description`, `value`) VALUES
(1, 'seo_description', 'Domyślna wartość meta tagu \"description\"', 'Instalka CMS Lemonade Studio'),
(2, 'seo_keywords', 'Domyślna wartość meta tagu \"keywords\"', NULL),
(3, 'seo_title', 'Domyślna wartość meta tagu \"title\"', 'Medokum / profesjonalne tłumaczenia dokumentacji medycznej'),
(4, 'email_to_contact', 'Adres/Adresy e-mail (oddzielone przecinkiem) na które wysyłane są wiadomości z formularza kontaktowego', 'kasia@mastermindtranslations.co.uk'),
(5, 'google_recaptcha_secret_key', 'Google reCAPTCHA - secret key', NULL),
(6, 'google_recaptcha_site_key', 'Google reCAPTCHA - site key', NULL),
(7, 'translate_page_limit', 'Limit tłumaczonych stron', '10'),
(8, 'delivery_cost', 'Cena dostawy', '10');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `label` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_width` int(11) NOT NULL,
  `photo_height` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `slider_photo`
--

CREATE TABLE `slider_photo` (
  `id` int(11) NOT NULL,
  `slider_id` int(11) DEFAULT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salt` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json_array)',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `salt`, `password`, `active`, `last_login`, `roles`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'biuro@lemonadestudio.pl', 'e40e0bc6f3ebacbed6e0e19b772845a8', 'Rd/3Txoa82/D9hi5AUbzSaMo0mR7YPs5tVG7TwD12SREfgob9BnzCItP1AhQxf7gi/IHE67DdDYPbMWPZIAROg==', 1, NULL, '[\"ROLE_ADMIN\",\"ROLE_ALLOWED_TO_SWITCH\",\"ROLE_USER\"]', '2015-07-31 13:46:34', NULL),
(2, 'tj', 'tomek@lemonadestudio.pl', 'eb4c9285d2177a7b763f33d3dfb26708', '3T10sVpadabvQNRDpxzsDZ1AtfFPX7PnTqo7SXdD3RKCtLKFomW68Tg4+zPEs+2saOHLgJoWx+Tj6UljBDx/pQ==', 1, NULL, '[\"ROLE_USER\"]', '2017-05-05 11:11:30', NULL),
(3, 'medokum', 'medokum@medokum.pl', 'f38f48f5e130f8d75ca503c2ef9a9ddc', 'ghnahsk0fEowLAUlofF/OglQd0mDjhrHxvjuYxzW/FHTjbV9T60ut9XyMnf85E8LJyA18NLfOhmBy82V2t3jjg==', 1, NULL, '[\"ROLE_ADMIN\"]', '2017-06-23 15:58:44', NULL),
(4, 'kasia', 'kasia@mastermindtranslations.co.uk', 'c05a528db07b4e799ed59bb42ef84014', 'zltEazO2HAXD6QD4T0Crum29LSnrGM8EvGHroll0L9VNhOYA8CtS/To467U56Q4c3NEi42Y2cjBy8SbbSv+4cw==', 1, NULL, '[\"ROLE_ADMIN\"]', '2017-07-14 15:49:31', NULL);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `disabled_dates`
--
ALTER TABLE `disabled_dates`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `discount`
--
ALTER TABLE `discount`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_E8FF75CC12469DE2` (`category_id`);

--
-- Indeksy dla tabeli `faq_categories`
--
ALTER TABLE `faq_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8C9F36108D9F6D38` (`order_id`),
  ADD KEY `IDX_8C9F3610C54C8C93` (`type_id`);

--
-- Indeksy dla tabeli `files_types`
--
ALTER TABLE `files_types`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `gallery_photo`
--
ALTER TABLE `gallery_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F02A543B4E7AF8F` (`gallery_id`);

--
-- Indeksy dla tabeli `index_content`
--
ALTER TABLE `index_content`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_34666CDCEA750E8` (`label`);

--
-- Indeksy dla tabeli `mailtemplates`
--
ALTER TABLE `mailtemplates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_9C3FF9A6EA750E8` (`label`);

--
-- Indeksy dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D754D550727ACA70` (`parent_id`);

--
-- Indeksy dla tabeli `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_1DD399504E7AF8F` (`gallery_id`);

--
-- Indeksy dla tabeli `opinions`
--
ALTER TABLE `opinions`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_E52FFDEE30602CA9` (`kind_id`),
  ADD KEY `IDX_E52FFDEE497B19F9` (`priority_id`);

--
-- Indeksy dla tabeli `orders_kind`
--
ALTER TABLE `orders_kind`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `orders_priority`
--
ALTER TABLE `orders_priority`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_140AB6204E7AF8F` (`gallery_id`);

--
-- Indeksy dla tabeli `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_9F74B898EA750E8` (`label`);

--
-- Indeksy dla tabeli `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_CFC71007EA750E8` (`label`);

--
-- Indeksy dla tabeli `slider_photo`
--
ALTER TABLE `slider_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9203C87C2CCC9638` (`slider_id`);

--
-- Indeksy dla tabeli `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `disabled_dates`
--
ALTER TABLE `disabled_dates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `discount`
--
ALTER TABLE `discount`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT dla tabeli `faq_categories`
--
ALTER TABLE `faq_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `file`
--
ALTER TABLE `file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT dla tabeli `files_types`
--
ALTER TABLE `files_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT dla tabeli `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `gallery_photo`
--
ALTER TABLE `gallery_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `index_content`
--
ALTER TABLE `index_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `mailtemplates`
--
ALTER TABLE `mailtemplates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `opinions`
--
ALTER TABLE `opinions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT dla tabeli `orders_kind`
--
ALTER TABLE `orders_kind`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `orders_priority`
--
ALTER TABLE `orders_priority`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT dla tabeli `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `slider_photo`
--
ALTER TABLE `slider_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `faq`
--
ALTER TABLE `faq`
  ADD CONSTRAINT `FK_E8FF75CC12469DE2` FOREIGN KEY (`category_id`) REFERENCES `faq_categories` (`id`);

--
-- Ograniczenia dla tabeli `file`
--
ALTER TABLE `file`
  ADD CONSTRAINT `FK_8C9F36108D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `FK_8C9F3610C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `files_types` (`id`);

--
-- Ograniczenia dla tabeli `gallery_photo`
--
ALTER TABLE `gallery_photo`
  ADD CONSTRAINT `FK_F02A543B4E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  ADD CONSTRAINT `FK_D754D550727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `menu_item` (`id`);

--
-- Ograniczenia dla tabeli `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `FK_1DD399504E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`);

--
-- Ograniczenia dla tabeli `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `FK_E52FFDEE30602CA9` FOREIGN KEY (`kind_id`) REFERENCES `orders_kind` (`id`),
  ADD CONSTRAINT `FK_E52FFDEE497B19F9` FOREIGN KEY (`priority_id`) REFERENCES `orders_priority` (`id`);

--
-- Ograniczenia dla tabeli `page`
--
ALTER TABLE `page`
  ADD CONSTRAINT `FK_140AB6204E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`);

--
-- Ograniczenia dla tabeli `slider_photo`
--
ALTER TABLE `slider_photo`
  ADD CONSTRAINT `FK_9203C87C2CCC9638` FOREIGN KEY (`slider_id`) REFERENCES `slider` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
