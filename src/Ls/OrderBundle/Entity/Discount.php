<?php

namespace Ls\OrderBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Discount
 * @ORM\Table(name="discount")
 * @ORM\Entity
 */
class Discount
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="integer", length=11, nullable=true)
     * @var boolean
     */
    private $availableUses;
    
    /**
     * @ORM\Column(type="integer", length=11, nullable=true)
     * @var boolean
     */
    private $usesCount;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var boolean
     */
    private $value;
        
    /**
     * @ORM\Column(type="integer", length=11, nullable=true)
     * @var boolean
     */
    private $discount;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $endDate;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->availableUses = 1;
        $this->usesCount = 0;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set availableUses
     *
     * @param integer $availableUses
     * @return Discount
     */
    public function setAvailableUses($availableUses)
    {
        $this->availableUses = $availableUses;

        return $this;
    }

    /**
     * Get availableUses
     *
     * @return integer
     */
    public function getAvailableUses()
    {
        return $this->availableUses;
    }
    
    /**
     * Set usesCount
     *
     * @param integer $usesCount
     * @return Discount
     */
    public function setUsesCount($usesCount)
    {
        $this->usesCount = $usesCount;

        return $this;
    }

    /**
     * Get usesCount
     *
     * @return integer
     */
    public function getUsesCount()
    {
        return $this->usesCount;
    }
    
    /**
     * Set discount
     *
     * @param integer $discount
     * @return Discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return integer
     */
    public function getDiscount()
    {
        return $this->discount;
    }
    
    /**
     * Set value
     *
     * @param string $value
     * @return Discount
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return integer
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $created_at
     * @return Discount
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return Discount
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    public function __toString() {
        if (is_null($this->getValue())) {
            return 'NULL';
        }
        return $this->getValue();
    }
}