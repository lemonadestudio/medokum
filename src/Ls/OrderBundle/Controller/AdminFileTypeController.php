<?php

namespace Ls\OrderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ls\OrderBundle\Entity\FileType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Ls\OrderBundle\Form\FileTypeAdminType;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AdminFileTypeController extends Controller {
    private $pager_limit_name = 'admin_file_type_limit';

    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');

        $page = $request->query->get('order', 1);
        if ($session->has($this->pager_limit_name)) {
            $limit = $session->get($this->pager_limit_name);
        } else {
            $limit = 15;
            $session->set($this->pager_limit_name, $limit);
        }

        $query = $em->createQueryBuilder()
            ->select('e')
            ->from('LsOrderBundle:FileType', 'e')
            ->getQuery();
        
        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $page,
            $limit,
            array(
                'defaultSortFieldName' => 'e.created_at',
                'defaultSortDirection' => 'desc',
            )
        );
        $entities->setTemplate('LsCoreBundle:Backend:paginator.html.twig');
        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl('ls_admin_order'));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Lista typów plików', $this->get('router')->generate('ls_admin_order'));

        return $this->render('LsOrderBundle:AdminFileType:index.html.twig', array(
            'page' => $page,
            'limit' => $limit,
            'entities' => $entities,
        ));
    }

    public function newAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = new FileType();

        $form = $this->createForm(FileTypeAdminType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_file_type_new'),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz i wróć na listę'));
        $form->add('submit_and_new', SubmitType::class, array('label' => 'Zapisz i dodaj następny'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $sitemap = $this->get('ls_core.sitemap');
            $sitemap->generate();

            $this->get('session')->getFlashBag()->add('success', 'Dodanie typu zakończone sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_file_type_edit', array('id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_file_type'));
            }
            if ($form->get('submit_and_new')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_file_type_new'));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Typy dokumentów', $this->get('router')->generate('ls_admin_file_type'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_file_type_new'));

        return $this->render('LsOrderBundle:AdminFileType:new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsOrderBundle:FileType')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FileType entity.');
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', 'Usunięcie typu zakończone sukcesem.');

        return new Response('OK');
    }
    
    public function editAction(Request $request, $id) 
    {
        $em = $this->getDoctrine()->getManager();
        if ($request->request) {
            $status = $request->request->get('form_order')['status'];
        }
        
        $Order = $em->getRepository('LsOrderBundle:FileType')->find($id);
        $files = $em->createQueryBuilder()
                ->select('f')
                ->from('LsOrderBundle:FileType', 'f')
                ->getQuery()
                ->getResult();
        
        if (!$Order) {
            throw $this->createNotFoundException('Unable to find FileType entity.');
        }
        
        $form = $this->createForm(FileTypeAdminType::class, $Order);
        
        $form->handleRequest($request);
        if ($form->isValid()) {
            $data = $form->getData();
            
            $em->persist($Order);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', 'Aktualizacja typu dokumentu zakończona sukcesem.');
            
            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_file_type_edit', array('id' => $id)));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_file_type'));
            }
        }
        
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Wypełnij wszystkie pola.');
        }
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Typy dokumentów', $this->get('router')->generate('ls_admin_file_type'));
        $breadcrumbs->addItem($Order->__toString(), $this->get('router')->generate('ls_admin_file_type_show', array('id' => $Order->getId())));
        
        return $this->render('LsOrderBundle:AdminFileType:edit.html.twig', array(
            'entity' => $Order,
            'form' => $form->createView(),
            'files' => $files
        ));
    }

    public function batchAction(Request $request) {
        $ids = $request->request->get('ids');
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = 'Czy na pewno chcesz ';
            switch ($action) {
                case 'delete':
                    $message .= 'usunąć ';
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'element?';
                    break;
                case 2:
                case 3:
                case 4:
                    $message .= 'elementy?';
                    break;
                default:
                    $message .= 'elementów?';
                    break;
            }

            $breadcrumbs = $this->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
            $breadcrumbs->addItem('Typy dokumentów', $this->get('router')->generate('ls_admin_file_type'));
            $breadcrumbs->addItem('Potwierdzenie', $this->get('router')->generate('ls_admin_file_type_batch'));

            return $this->render('LsOrderBundle:AdminFileType:batch.html.twig', array(
                'message' => $message,
                'action' => $action,
                'ids' => implode(',', $ids),
            ));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_file_type'));
        }
    }

    public function batchExecuteAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $ids = explode(',', $request->request->get('ids'));
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = '';
            switch ($action) {
                case 'delete':
                    $message .= 'Usunięcie ';
                    $qb = $em->createQueryBuilder();
                    $query = $qb->select('e')
                        ->from('LsOrderBundle:FileType', 'e')
                        ->add('where', $qb->expr()->in('e.id', $ids))
                        ->getQuery();

                    $iterableResult = $query->iterate();
                    while (($row = $iterableResult->next()) !== false) {
                        $em->remove($row[0]);
                        $em->flush();
                    }
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'elementu ';
                    break;
                default:
                    $message .= 'elementów ';
                    break;
            }
            $message .= 'zakończone sukcesem ';

            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirect($this->generateUrl('ls_admin_file_type'));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_file_type'));
        }
    }

    public function setLimitAction(Request $request) {
        $session = $this->container->get('session');

        $limit = $request->request->get('limit');
        $session->set($this->pager_limit_name, $limit);

        return new Response('OK');
    }
}
