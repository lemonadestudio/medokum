<?php

namespace Ls\OrderBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class FileTypeType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('name', null, array(
                'label' => 'Nazwa',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Wypełnij pole'))
                )
            )
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\OrderBundle\Entity\FileType',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_file_type_type';
    }
}
