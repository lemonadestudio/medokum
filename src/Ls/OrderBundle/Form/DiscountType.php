<?php

namespace Ls\OrderBundle\Form;

use Ls\CoreBundle\Form\DataTransformer\DateTimeTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class DiscountType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder, $options);


        $builder->add('endDate', TextType::class, array(
            'label' => 'Data ważności',
            'attr' => array(
                'data-date-format' => 'DD.MM.YYYY HH:mm'
            ),
            'constraints' => array(
                new NotBlank(array('message' => 'Wypełnij pole'))
            )
        ));
        $builder->get('endDate')->addModelTransformer(new DateTimeTransformer());

        $builder->add('availableUses', null, array(
            'label' => 'Ilość dostępnych wykorzystań',
            'constraints' => array(
                new NotBlank(array('message' => 'Wypełnij pole'))
            )
        ));
        
        $builder->add('discount', null, array(
            'label' => 'Procent zniżki',
            'attr' => array(
                'min' => '0',
                'max' => '100',
            ),
            'constraints' => array(
                new NotBlank(array('message' => 'Wypełnij pole'))
            )
        ));
        
        $builder->add('value', null, array(
            'label' => 'Kod',
            'constraints' => array(
                new NotBlank(array('message' => 'Wypełnij pole'))
            )
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\OrderBundle\Entity\Discount',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_admin_discount';
    }
}
